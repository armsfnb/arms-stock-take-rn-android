import { AppRegistry, YellowBox } from 'react-native';
import App from './App';
import { Navigation } from 'react-navigation';

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
AppRegistry.registerComponent('ARMSStockTake', () => App);

// Navigation.registerComponent('ARMSStockTake', () => App);
// Navigation.startSingleScreenApp({
//   screen: {
//     screen: 'ARMSStockTake',
//     title: 'Welcome'
//   }
// });