/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  Alert,
  FlatList,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';

import { createStackNavigator } from 'react-navigation';
import Styles from './source/styles';

import HomeScreen from './source/module/mainScreen';

import StockTakeInfoScreen from './source/module/stockTakeInfoScreen';
import StockTakeScanScreen from './source/module/stockTakeScanScreen';

import PriceCheckScreen from './source/module/priceCheckScreen';

import ViewReportGenerateReportScreen from './source/module/viewReportGenerateReportScreen';
import ViewReportPickerScreen from './source/module/viewReportPickerScreen';
import ViewReportSTReportScreen from './source/module/viewReportSTReportScreen';
import ViewReportSTDetailsScreen from './source/module/viewReportSTDetailsScreen';

import ScannerScreen from './source/module/scannerScreen';
import PriceCheckScannerScreen from './source/module/priceCheckScannerScreen'

import SettingsMainScreen from './source/module/settingsMainScreen';
import SettingsExportFileFormatSetupScreen from './source/module/settingsExportFileFormatSetupScreen';
import SettingsClearDataScreen from './source/module/settingsClearDataScreen';
import SettingsClearDataSelectCriteriaScreen from './source/module/settingsClearDataSelectCriteriaScreen';
import SettingsClearDataByCriteriaScreen from './source/module/settingsClearDataByCriteriaScreen';

// import DownloadSampleScreen from './source/module/downloadSample';

const MainStack = createStackNavigator({
  Home: HomeScreen,

  StockTakeInfo: StockTakeInfoScreen,
  StockTakeScan: StockTakeScanScreen,

  PriceCheck: PriceCheckScreen,

  ViewReportGenerateReport: ViewReportGenerateReportScreen,
  ViewReportPicker: ViewReportPickerScreen,
  ViewReportSTReport: ViewReportSTReportScreen,
  ViewReportSTDetails: ViewReportSTDetailsScreen,

  SettingsMain: SettingsMainScreen,
  SettingsExportFileFormatSetup: SettingsExportFileFormatSetupScreen,
  SettingsClearData: SettingsClearDataScreen,
  SettingsClearDataSelectCriteria: SettingsClearDataSelectCriteriaScreen,
  SettingsClearDataByCriteria: SettingsClearDataByCriteriaScreen,

  // DownloadSample: DownloadSampleScreen
},
  {
    initialRouteName: 'Home',
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#eee'
      },
      headerTintColor: '#000',
      headerTintStyle: {
        fontWeight: 'bold',
      },
    }
  })

const RootStack = createStackNavigator({
  Main: MainStack,
  Scanner: ScannerScreen,
  PriceCheckScanner: PriceCheckScannerScreen,
},
  {
    mode: 'modal',
    headerMode: 'none',
  })

export default class App extends React.Component {
  render() {
    return <RootStack />
  }
}
