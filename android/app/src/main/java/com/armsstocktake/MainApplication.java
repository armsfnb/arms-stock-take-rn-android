package com.armsstocktake;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.rnfs.RNFSPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.chirag.RNMail.*;
// import com.chirag.RNMail.RNMail;
import org.reactnative.camera.RNCameraPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.rnziparchive.RNZipArchivePackage;

import com.reactnativedocumentpicker.ReactNativeDocumentPicker;

import com.reactlibrary.mailcompose.RNMailComposePackage;

import com.rnfs.RNFSPackage;

import com.learnium.RNDeviceInfo.RNDeviceInfo;

import com.rn.full.screen.FullScreenModule;
// import ca.jaysoo.extradimensions.ExtraDimensionsPackage;

// import net.zubricky.AndroidKeyboardAdjust.AndroidKeyboardAdjustPackage;

import java.util.Arrays;
import java.util.List;

import org.pgsqlite.SQLitePluginPackage;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
        new SQLitePluginPackage(),
        new MainReactPackage(),
        // new RNFSPackage(),
        new RNFetchBlobPackage(),
        new RNMail(),
        new RNCameraPackage(),
        new RNZipArchivePackage(),
        new ReactNativeDocumentPicker(),
        new RNFSPackage(),
        new RNDeviceInfo(),
        new RNMailComposePackage(),
        new FullScreenModule()
        // new ExtraDimensionsPackage()


        // new AndroidKeyboardAdjustPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
