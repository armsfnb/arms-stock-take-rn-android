import { StyleSheet } from 'react-native';

const Styles = StyleSheet.create({
  // Header
  headerButton: {
    width: 30, height: 30, tintColor: '#157efb'
  },
  headerButtonPosition: {
    width: 50, alignSelf: 'center',
  },
  headerTextPosition: {
    width: 40, alignSelf: 'flex-start', fontSize: 18, color: '#157efb',
  },
  // Main Screen
  mainView: {
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  mainViewHeader: {
    backgroundColor: 'white',
    height: 30
  },
  mainList: {
    flex: 1,
    width: '100%'
  },
  mainListItem: {
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'row',
    paddingVertical: 8,
    paddingLeft: 20,
  },
  mainListImage: {
    width: 30,
    height: 30,
    marginVertical: 7,
    marginHorizontal: 8,
  },
  mainListText: {
    fontSize: 16,
    height: 45,
    padding: 10,
  },
  mainReadCSVFileView: {
    flex: 1,
    backgroundColor: 'transparent',
    // opacity: 0.0,
    position: 'absolute',
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  mainActivityIndicator: {
    backgroundColor: 'white',
    width: 100,
    height: 75,
    alignItems: 'center',
    justifyContent: 'center'
  },
  mainReadCSVText: {
    color: 'black',
    backgroundColor: 'white',
    opacity: 0.7,
    fontSize: 16,
    // width: 150,
    // height: 75,
    paddingHorizontal: 40,
    paddingVertical: 10,
    textAlign: 'center',
  },
  // Stock Take Info Screen
  StockTakeInfoView: {
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  StockTakeInfoRow: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    height: 65,
    paddingVertical: 8,
    paddingLeft: 30
  },
  lblStockTakeInfo: {
    fontSize: 16,
    width: 110,
    paddingVertical: 10
  },
  txtStockTakeInfo: {
    flex: 1,
    marginRight: 30,
    paddingLeft: 5,
    borderWidth: 0.3,
    borderColor: '#ccc',
    backgroundColor: '#f8f8f8',
    height: 42.5,
    fontSize: 14,
  },
  btnStockTakeDate: {
    flex: 1,
    marginRight: 30,
    backgroundColor: '#157efb',
    paddingVertical: 8,
    marginVertical: 4
  },
  strStockTakeDate: {
    color: 'white',
    alignSelf: 'center'
  },
  switchIsQtyOne: {
    flex: 1,
    justifyContent: 'flex-end',
    marginRight: 30,
    paddingTop: 14,
  },
  btnStockTakeInfo: {
    color: 'white',
    fontSize: 14,
    backgroundColor: '#157efb',
    marginTop: 10,
    marginBottom: 30,
    paddingVertical: 8,
    // button
    alignSelf: 'center',
    textAlign: 'center',
    width: '50%',
  },
  headerAutosave: {
    fontSize: 12,
    textAlign: 'center',
    marginTop: 10,
    paddingVertical: 8,
    paddingHorizontal: 30,
  },
  saveInfoText: {
    height: 25,
    fontSize: 14,
    textAlign: 'center',
    color: 'green',
  },
  // ScannerScreen
  scannerContainer: {
    flex: 1,
    // flexDirection: 'row',
  },
  scannerPreview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  scannerHeaderFooter: {
    backgroundColor: 'black',
    opacity: 0.7,
    position: 'absolute',
    width: '100%',
    height: 80,
  },
  headerText: {
    color: 'white',
    fontSize: 24,
    position: 'absolute',
    width: '100%',
    textAlign: 'center',
    bottom: 18,
  },
  headerView: {
    top: 0
  },
  footerText: {
    color: 'white',
    fontSize: 18,
    position: 'absolute',
    width: '80%',
    left: '10%',
    textAlign: 'center',
    top: 6,
  },
  footerView: {
    bottom: 0
  },
  scannerReturn: {
    position: 'absolute',
    width: 25,
    height: 25,
    top: 35,
    left: 20,
  },
  scannerHeaderCross: {
    tintColor: 'white',
    position: 'absolute',
    width: 25,
    height: 25,
  },
  // Price Check Screen
  // 1. Scan product
  scanProductView: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    height: 55,
    paddingVertical: 8,
    paddingLeft: '7.5%'
    // paddingLeft: 30
  },
  lblPriceCheckScanProduct: {
    fontSize: 16,
    width: 130,
    paddingVertical: 10
  },
  txtPriceCheckScanProduct: {
    flex: 1,
    marginRight: 30,
    paddingLeft: 5,
    borderWidth: 0.3,
    borderColor: '#ccc',
    backgroundColor: '#f8f8f8',
    height: 42.5,
    fontSize: 14,
  },
  // 2. Price Check
  priceCheckView: {
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  priceCheckDetailView: {
    width: '85%',
  },
  // 2a. Portrait
  priceCheckRow: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    height: 25,
  },
  lblPriceCheck: {
    flex: 1,
    fontSize: 14,
  },
  sellingPriceRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 100,
  },
  lblSellingPrice: {
    fontSize: 48,
    fontWeight: 'bold',
    backgroundColor: '#D7E6FE',
    width: '100%',
    textAlign: 'center',
    paddingVertical: 13,
  },
  lblPriceNotFound: {
    fontSize: 36,
    fontWeight: 'bold',
    backgroundColor: '#FED7E6',
    width: '100%',
    textAlign: 'center',
    paddingVertical: 19,
  },
  // 2b. Landscape
  priceCheckRowLandscape: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    // height: 20,
  },
  lblPriceCheckLandscape: {
    flex: 1,
    fontSize: 12,
  },
  lblPriceCheckProdLandscape: {
    // flex: 1,
    fontSize: 12,
    fontWeight: 'bold',
  },
  // 3. DB date & SKU records
  databaseRow: {
    backgroundColor: 'white',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    height: 60,
    width: '80%',
  },
  lblDatabase: {
    fontSize: 12,
    textAlign: 'center',
  },
  // View Report Generate Report Screen
  VRGenerateReportView: {
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  VRGenerateReportRow: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    height: 55,
    paddingVertical: 8,
    paddingLeft: 30
  },
  lblVRGenerateReport: {
    fontSize: 16,
    width: 110,
    paddingVertical: 5
  },
  btnVRGenerateReportCriteria: {
    flex: 1,
    marginRight: 30,
    backgroundColor: '#e8e8e8',
    paddingVertical: 8,
  },
  strVRGenerateReportCriteria: {
    color: '#111',
    alignSelf: 'center'
  },
  btnVRGenerateReportDate: {
    flex: 1,
    marginRight: 30,
    backgroundColor: '#157efb',
    paddingVertical: 8,
  },
  strVRGenerateReportDate: {
    color: 'white',
    alignSelf: 'center'
  },
  btnVRGenerateReport: {
    color: 'white',
    fontSize: 14,
    backgroundColor: '#157efb',
    marginTop: 10,
    marginBottom: 30,
    paddingVertical: 8,
    // button
    alignSelf: 'center',
    textAlign: 'center',
    width: '50%',
  },
  // View Report Stock Take Report Screen
  viewReportContainer: {
    flex: 1,
    paddingTop: 10
  },
  sectionListHeader: {
    paddingHorizontal: 10,
    paddingVertical: 2,
    fontSize: 15,
    fontWeight: 'bold',
    backgroundColor: 'rgba(247,247,247,1.0)',
  },
  sectionListItem: {
    flex: 1,
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  sectionListCode: {
    flex: 5,
  },
  sectionListQty: {
    flex: 2,
    textAlign: 'center'
  },
  // View Report Stock Take Details Screen
  stDetailsRow: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    height: 45,
    paddingVertical: 8,
    // paddingLeft: 30,
    marginLeft: '10%',
    width: '80%'
  },
  stDetailsInfo: {
    flex: 1,
    marginRight: 15,
    paddingLeft: 5,
    paddingVertical: 5,
    fontSize: 16,
    color: '#214281',
  },
  txtStDetails: {
    flex: 1,
    marginRight: '60%',
    paddingLeft: 5,
    borderWidth: 0.3,
    borderColor: '#ccc',
    backgroundColor: '#f8f8f8',
    height: 42.5,
    fontSize: 14,
  },
  btnStDetails: {
    // flex: 1,
    // flexDirection: 'column',
    color: 'white',
    fontSize: 14,
    backgroundColor: '#157efb',
    paddingVertical: 8,
    paddingHorizontal: '10%',
    marginHorizontal: 15,
    textAlign: 'center',
  },
  exportFileFormatSectionTitle: {
    // backgroundColor: 'orange',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingTop: 25,
    paddingBottom: 10,
    paddingLeft: '10%',
    width: '100%',
  },
  exportFileFormatTitleLbl: {
    fontSize: 16,
  },
  exportFileFormatHeaderPreview: {
    backgroundColor: '#D7E6FE',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    alignSelf: 'center',
    padding: 8,
    height: 50,
    width: '80%',
  },
  exportFileFormatBtnClear: {
    paddingTop: 10,
    alignSelf: 'center',
    color: '#157efb',
  },
  exportFileFormatHeaderLibrary: {
    // backgroundColor: 'yellow',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    alignSelf: 'center',
    width: '80%',
  },
  exportFileFormatBtnLibrary: {
    color: 'white',
    fontSize: 14,
    backgroundColor: '#157efb',
    paddingVertical: 8,
    paddingHorizontal: '10%',
    margin: 5,
    textAlign: 'center',
  },
  exportFileFormatDelimiterRow: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    alignSelf: 'center',
    backgroundColor: 'white',
    width: '80%',
  },
  // Settings Clear Data
  ClearDataViewHeader: {
    backgroundColor: 'white',
    height: 60,
  },
  ClearDataRow: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    height: 55,
    paddingVertical: 8,
    marginBottom: 30,
    paddingLeft: 60
  },
  lblClearData: {
    fontSize: 16,
    width: 150,
    // width: '30%',
    paddingVertical: 5
  },
  switchIsClearAll: {
    flex: 1,
    justifyContent: 'flex-end',
    marginRight: 60,
  },
  // Button
  btnEnabled: {
    opacity: 1.0
  },
  btnDisabled: {
    opacity: 0.5,
  },
  // Slider
  slider: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  }
})

export default Styles;