import React, { Component } from 'react';
import {
  Alert,
  AsyncStorage,
  ToastAndroid,
} from 'react-native';
import SQLiteStorage from 'react-native-sqlite-storage';
import Moment from 'moment';
SQLiteStorage.DEBUG(true);
// var database_name = "test.db";//数据库文件  
// var database_version = "1.0";//版本号  
// var database_displayname = "MySQLite";
// var database_size = -1;//-1应该是表示无限制  
var db;
export default class SQLite extends Component {
  componentWillUnmount() {
    if (db) {
      this._successCB('close');
      db.close();
    } else {
      console.log("SQLiteStorage not open");
    }
  }

  open() {
    db = SQLiteStorage.openDatabase({ name: 'test.db', createFromLocation: '~stockTakeDatabase.db' },
      // database_name,
      // database_version,
      // database_displayname,
      // database_size,
      () => {
        this._successCB('open');
      },
      (err) => {
        this._errorCB('open', err);
      });
    return db;
  }

  // createTable() {
  //   if (!db) {
  //     this.open();
  //   }
  //   //创建用户表  
  //   db.transaction((tx) => {
  //     tx.executeSql('CREATE TABLE IF NOT EXISTS USER(' +
  //       'id INTEGER PRIMARY KEY  AUTOINCREMENT,' +
  //       'name varchar,' +
  //       'age VARCHAR,' +
  //       'sex VARCHAR,' +
  //       'phone VARCHAR,' +
  //       'email VARCHAR,' +
  //       'qq VARCHAR)'
  //       , [], () => {
  //         this._successCB('executeSql');
  //       }, (err) => {
  //         this._errorCB('executeSql', err);
  //       });
  //   }, (err) => {//所有的 transaction都应该有错误的回调方法，在方法里面打印异常信息，不然你可能不会知道哪里出错了。  
  //     this._errorCB('transaction', err);
  //   }, () => {
  //     this._successCB('transaction');
  //   })
  // }

  // deleteData() {
  //   if (!db) {
  //     this.open();
  //   }
  //   db.transaction((tx) => {
  //     tx.executeSql('delete from user', [], () => {

  //     });
  //   });
  // }

  deleteStockTakeData() {
    if (!db) {
      this.open();
    }
    db.transaction((tx) => {
      tx.executeSql('delete from stockTakeRecord', [], () => {
      });
    });
  }

  deleteSKUItems() {
    if (!db) {
      this.open();
    }
    db.transaction((tx) => {
      tx.executeSql('delete from SKUitem', [], () => {
      });
    });
  }

  // dropTable() {
  //   db.transaction((tx) => {
  //     tx.executeSql('drop table user', [], () => {

  //     });
  //   }, (err) => {
  //     this._errorCB('transaction', err);
  //   }, () => {
  //     this._successCB('transaction');
  //   });
  // }


  async saveDbImportDate(dateString) {
    try {
      await AsyncStorage.setItem('@dbDate', dateString);
    } catch (error) {
      console.log("Error saving date" + error)
    }
  }

  insertStockTakeData(stockTakeData) {
    // let len = userData.length;
    if (!db) {
      this.open();
    }

    let branch = stockTakeData.branch
    let date = stockTakeData.date
    let dateSaved = stockTakeData.dateSaved
    let itemCode = stockTakeData.itemCode
    let itemQty = stockTakeData.itemQty
    let location = stockTakeData.location
    let shelf = stockTakeData.shelf
    let user = stockTakeData.user

    let sqlQuery = 'INSERT INTO stockTakeRecord(branch,dateSaved,dateStockTake,itemCode,itemQty,location,shelf,user) ' +
      'VALUES(?,?,?,?,?,?,?,?);'
    db.transaction((tx) => {
      tx.executeSql(sqlQuery, [branch, dateSaved, date, itemCode, itemQty, location, shelf, user], () => {
      }, (err) => {
        console.log(err);
      });
    }, (error) => {
      this._errorCB('transaction', error);
      ToastAndroid.show("数据插入失败", ToastAndroid.SHORT);
    }, () => {
      this._successCB('transaction insert data');
      // ToastAndroid.show("成功插入 1 条用户数据", ToastAndroid.SHORT);
      ToastAndroid.show("New Stock Take record saved.", ToastAndroid.SHORT);
    });
  }

  insertSKUItems(skuList) {
    // let len = userData.length;
    if (!db) {
      this.open();
    }

    for (i = 0; i < skuList.length; i++) {
      const SKUitem = skuList[i]

      let skuItemId = SKUitem[0]
      let armsCode = SKUitem[1]
      let artno = SKUitem[2]
      let mcode = SKUitem[3]
      let barcode = SKUitem[4]

      let prodDesc = SKUitem[5]
      let rcptDesc = SKUitem[6]
      let sellingPrice = SKUitem[7]
      let costPrice = SKUitem[8]
      let priceType = SKUitem[9]

      let skuType = SKUitem[10]
      let department = SKUitem[11]
      let category = SKUitem[12]
      let skuOrPrc = SKUitem[13]
      let uomCode = SKUitem[14]

      let fraction = SKUitem[15]
      let inputTax = SKUitem[16]
      let outputTax = SKUitem[17]
      let inclTax = SKUitem[18]
      let scaleType = SKUitem[19]

      let active = SKUitem[20]
      let brand = SKUitem[21]
      let vendor = SKUitem[22]
      let parentArmsCode = SKUitem[23]
      let parentArtno = SKUitem[24]

      let parentMcode = SKUitem[25]

      let skuInfo = [active, armsCode, artno, barcode, brand, category, costPrice, department, fraction, inclTax,
        inputTax, mcode, outputTax, parentArmsCode, parentArtno, parentMcode, priceType, prodDesc, rcptDesc, scaleType,
        sellingPrice, skuItemId, skuOrPrc, skuType, uomCode, vendor]

      let tableColumns = `active,armsCode,artno,barcode,brand,category,costPrice,department,fraction,inclusiveTax,` +
        `inputTax,mcode,outputTax,parentArmsCode,parentArtno,parentMcode,priceType,productDescription,receiptDescription,scaleType,` +
        `sellingPrice,skuItemID,skuOrPrc,skuType,uomCode,vendor`

      let sqlQuery = `INSERT INTO SKUItem(${tableColumns}) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);`
      db.transaction((tx) => {
        tx.executeSql(sqlQuery, skuInfo, () => {
          if (skuItemId == skuList[skuList.length - 1][0]) {
            const importDate = Moment(new Date()).format('DD/MM/YYYY')
            console.log("done")
            Alert.alert(
              'Import Successful',
              `Database imported on: ${importDate}\nNo of SKU records: ${skuList.length}`,
              [{ text: 'Close', style: 'default' }],
              { cancelable: true }
            )
            this.saveDbImportDate(importDate)
          }
        }, (err) => {
          console.log(err);
        });
      }, (error) => {
        this._errorCB('transaction', error);
        // ToastAndroid.show(error.message, ToastAndroid.SHORT);
        // ToastAndroid.show("Failed to import new SKU item", ToastAndroid.SHORT);
      }, () => {
        this._successCB('transaction insert data');
        // // ToastAndroid.show("成功插入 1 条用户数据", ToastAndroid.SHORT);
        // // ToastAndroid.show("New SKU item imported.", ToastAndroid.SHORT);
      });
    }
  }

  close() {
    if (db) {
      this._successCB('close');
      db.close();
    } else {
      console.log("SQLiteStorage not open");
    }
    db = null;
  }

  _successCB(name) {
    console.log("SQLiteStorage " + name + " success");
  }

  _errorCB(name, err) {
    console.log("SQLiteStorage " + name);
    console.log(err);
  }

  render() {
    return null;
  }
};  