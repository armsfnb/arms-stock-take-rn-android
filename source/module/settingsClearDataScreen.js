import React, { Component } from 'react';
import {
  Platform,
  View,
  Text,
  Alert,
  Switch,
  AsyncStorage,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Styles from '../styles'
import SQLite from './sqlite'

var sqLite = new SQLite();

export default class SettingsClearDataScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isClearAll: true,
      clearRecordsBy: 'Branch',
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
    }
  }

  static navigationOptions = ({ navigation, navigationOptions }) => {
    const params = navigation.state.params || {};
    return {
      title: 'Clear Records',
    };
  }

  componentWillMount() {
    sqLite.open().transaction((tx) => {
      tx.executeSql("select itemCode from stockTakeRecord limit 1", [], (tx, results) => {
        // second layer blocking
        if (results.rows.length == 0) {
          Alert.alert(
            'No Stock Take Record',
            'Database is now empty.',
            [{ text: 'Close', style: 'default', onPress: this.props.navigation.goBack() }],
            { cancelable: false }
          )
        }

      });
    }, (error) => {
      console.log(error);
    });
  }

  componentDidMount() {
  }

  componentWillUnmount() {
  }

  changeSwitchValue = (value) => {
    return (
      this.setState({ isClearAll: !this.state.isClearAll })
    )
  }

  clearAllRecords() {
    // Clear records
    sqLite.deleteStockTakeData()
    this.removeStockTakeList()
    Alert.alert(
      'Clear All', 'All Stock Take records have been cleared.',
      [{ text: 'OK', onPress: () => this.resetSTAlert() }],
      { cancelable: false }
    )
  }

  resetSTAlert() {
    this.props.navigation.navigate('Home')
    Alert.alert(
      'Reset Stock Take Settings?', 'This action cannot be undone.',
      [
        { text: 'Cancel', style: 'cancel' },
        { text: 'Reset', style: 'destructive', onPress: () => this.resetStockTakeSettings() }
      ],
      { cancelable: false }
    )
  }

  async removeStockTakeList() {
    // Remove all (temporary)
    try {
      await AsyncStorage.removeItem('@branchList')
      await AsyncStorage.removeItem('@dateList')
      await AsyncStorage.removeItem('@locationList')
      await AsyncStorage.removeItem('@shelfList')
      await AsyncStorage.removeItem('@userList')
    } catch (error) {
      console.log("Error removing data" + error)
    }
  }

  async resetStockTakeSettings() {
    // alert('Reset Pressed')
    Alert.alert(
      'Reset Stock Take Settings', 'Done.',
      [{ text: 'OK' }],
      { cancelable: false }
    )
    try {
      await AsyncStorage.removeItem('@stockTakeBranch');
      await AsyncStorage.removeItem('@stockTakeLocation');
      await AsyncStorage.removeItem('@stockTakeShelf');
      await AsyncStorage.removeItem('@stockTakeUser');
      await AsyncStorage.removeItem('@stockTakeIsQtyOne');
    } catch (error) {
      console.log("Error removing  data" + error)
    }
  }

  onLayout(e) {
    const { width, height } = Dimensions.get('window')
    this.setState({ screenWidth: width, screenHeight: height })
  }

  // _onPress = () => {
  //   this.props.navigation.navigate('SettingsClearDataSelectCriteria')
  // };

  render() {
    const { navigation } = this.props;

    var landscape = this.state.screenWidth > this.state.screenHeight
    var element = navigation.getParam('clearRecordsBy', 'Branch')
    this.state.clearRecordsBy = element

    return (
      <View
        onLayout={this.onLayout.bind(this)}
        style={Styles.StockTakeInfoView}
      >
        <View style={[Styles.ClearDataViewHeader, landscape ? { height: 20 } : null]}>
        </View>

        {/* Clear all */}
        <View style={[Styles.ClearDataRow, { marginBottom: 0 }, landscape ? { height: 40 } : null]}>
          <Text style={Styles.lblClearData}>
            Clear All
          </Text>
          <Switch
            style={Styles.switchIsClearAll}
            onValueChange={() => this.changeSwitchValue()}
            value={this.state.isClearAll}
          />
        </View>

        {/* Clear Records By */}
        {!this.state.isClearAll ?
          <View style={[Styles.ClearDataRow, landscape ? { marginBottom: 10 } : null]}>
            <Text style={[Styles.lblClearData, landscape ? { width: '60%' } : null]}>
              Clear Records by
            </Text>

            <TouchableOpacity
              style={[Styles.btnStockTakeDate, Styles.switchIsClearAll]}
              onPress={() => this.props.navigation.navigate('SettingsClearDataSelectCriteria')}
            >
              <Text style={Styles.strStockTakeDate}>{this.state.clearRecordsBy}</Text>
            </TouchableOpacity>

          </View>
          :
          <View style={[Styles.ClearDataRow, landscape ? { marginBottom: 10 } : null]}></View>
        }

        {/* <View Style={{ flex: 1, paddingVertical: 80, backgroundColor: 'red' }}></View> */}

        {/* Confirm Clear */}
        <TouchableOpacity
          onPress={() => {
            if (this.state.isClearAll) {
              Alert.alert(
                'Are you sure?', 'This action cannot be undone.',
                [
                  { text: 'Cancel', style: 'cancel' },
                  { text: 'Clear', style: 'destructive', onPress: () => this.clearAllRecords() }
                ],
                { cancelable: false }
              )
            } else {
              this.props.navigation.navigate('SettingsClearDataByCriteria', {
                criteria: this.state.clearRecordsBy
              })
            }
          }}
        >
          <Text
            style={[Styles.btnStockTakeInfo, { width: 200 },
            landscape ? { marginTop: 0 } : null]}
          >
          Confirm
          </Text>
        </TouchableOpacity>

      </View >
    );
  }
}
