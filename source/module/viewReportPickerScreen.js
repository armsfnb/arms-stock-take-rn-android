/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  Text,
  View,
  Image,
  Alert,
  FlatList,
  AsyncStorage,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Styles from '../styles'

export default class viewReportPickerScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      stateData: [],
      newData: [],
      criteria: ''
    }
    this.fetchStockTakeInfoList()
  }

  static navigationOptions = ({ navigation, navigationOptions }) => {
    const { params } = navigation.state

    return {
      title: params ? `Select ${params.criteria}` : 'Select Criteria',
      // All
      headerRight: (
        <TouchableOpacity onPress={() => {
          navigation.navigate('ViewReportGenerateReport', {
            selectedElement: 'All',
            selectedCriteria: params.criteria,
          })
        }}>
          <View style={Styles.headerButtonPosition}>
            <Text style={Styles.headerTextPosition}>All</Text>
          </View>
        </TouchableOpacity>
      ),
    };
  };

  async fetchStockTakeInfoList() {
    const { params } = this.props.navigation.state
    var stateDataList

    var fetchKey
    switch (params.criteria) {
      case 'Branch': fetchKey = '@branchList'; break
      case 'Location': fetchKey = '@locationList'; break
      case 'Shelf': fetchKey = '@shelfList'; break
      case 'User': fetchKey = '@userList'; break
      default: break
    }
    try {
      const stateDataString = await AsyncStorage.getItem(fetchKey);
      stateDataList = stateDataString != null ? JSON.parse(stateDataString) : []
      this.state.stateData = stateDataList.sort()
    } catch (error) {
      console.log("Error retrieving data" + error)
    }

    var len
    if (this.state.stateData != null) {
      len = this.state.stateData.length
    } else {
      len = 0
    }
    // let len = this.state.stateData.length
    var newDataList = []

    for (let i = 0; i < len; i++) {
      newDataList.push({ key: this.state.stateData[i] })
    }

    this.setState({
      newData: newDataList
    })
  }

  _onPress = (rowIndex) => {
    this.props.navigation.navigate('ViewReportGenerateReport', {
      selectedElement: this.state.stateData[rowIndex],
      selectedCriteria: this.state.criteria,
    })
  };

  render() {
    const { navigation } = this.props;
    const criteria = navigation.getParam('criteria', '')
    this.state.criteria = criteria

    return (
      <View style={Styles.mainView}>
        <FlatList style={Styles.mainList}
          data={this.state.newData}
          renderItem={({ item, index, separators }) => (
            <TouchableHighlight
              onPress={() => this._onPress(index)}
              onShowUnderlay={separators.highlight}
              onHideUnderlay={separators.unhighlight}
            >
              <View style={Styles.mainListItem}>
                <Text style={Styles.mainListText}>
                  {item.key}
                </Text>
              </View>
            </TouchableHighlight>
          )}
        />
      </View>
    );
  }
}


