/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  Text,
  View,
  Image,
  Alert,
  FlatList,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight,
  ActivityIndicator
} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import RNFetchBlob from 'react-native-fetch-blob'

import DeviceInfo from 'react-native-device-info'
// import ExtraDimensions from 'react-native-extra-dimensions-android'

import Styles from '../styles'
import SQLite from './sqlite'

var sqLite = new SQLite();
var shouldDisableSetting = false

const TOTAL_COLUMN = 26
const NQUIRE_PRICE_CHECKER = "NQuire 300"

const IS_NQUIRE_PRICE_CHECKER = (DeviceInfo.getModel() == "NQuire 300")


export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      importString: '',
      isImporting: false,
      priceCheck: false,
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
    }
  }

  static navigationOptions = ({ navigation, navigationOptions }) => {
    const params = navigation.state.params || {};

    return {
      title: 'Main',
      headerRight:
        <TouchableOpacity
          disabled={params.shouldDisableSetting}
          onPress={() => navigation.navigate('SettingsMain')}
        >
          <View style={Styles.headerButtonPosition}>
            <Image
              source={require('../../Assets/settings.png')}
              style={Styles.headerButton}
            />
          </View>
        </TouchableOpacity>
    };
  };

  componentDidMount() {
    this.props.navigation.setParams({ handleAlert: this.featureComingSoon });
  }

  featureComingSoon = () => {
    Alert.alert(
      'Coming Soon', 'This feature will be available soon.',
      [{ text: 'Close', style: 'cancel' }],
      { cancelable: false }
    )
  }

  showDocumentPicker() {
    DocumentPicker.show({
      filetype: [DocumentPickerUtil.allFiles()],
    }, (error, res) => {
      // Android
      if (res != null) {
        this.readCSVFile(res.uri)
        // if (res.type != 'text/csv') {
        //   Alert.alert(
        //     // 'Error', 'Please select CSV file that contains SKU data exported from ARMS Backend module.',
        //     'Error', 'error error error error error.',
        //     [{ text: 'Close', style: 'default' }],
        //     { cancelable: false }
        //   )
        // } else {
        //   this.readCSVFile(res.uri)
        // }
      } else {
        // No file selected
        // Alert.alert(
        //   'Error', 'No file selected.',
        //   [{ text: 'Close', style: 'default' }],
        //   { cancelable: false }
        // )
      }
    });
    // Note: Document Picker: https://github.com/Elyx0/react-native-document-picker
  }

  readCSVFile(filePath) {
    let data = ''
    RNFetchBlob.fs.readStream(
      filePath, 'utf8')
      .then((ifstream) => {
        ifstream.open()
        ifstream.onData((chunk) => {
          data += chunk
          this.setState({ importString: data, isImporting: true })
        })
        ifstream.onError((err) => {
          console.log('oops', err)
        })
        ifstream.onEnd(() => {
          this.importSKUData();
        })
      })
    // Note: Read CSV File: https://github.com/joltup/react-native-fetch-blob
  }

  importSKUData() {
    const dataList = this.state.importString.split('\n')
    const columnNumber = dataList[0].split(',').length

    var hasDuplicateEntry = false

    if (columnNumber != TOTAL_COLUMN) {
      Alert.alert(
        'Error', 'Please select CSV file that contains SKU data exported from ARMS Backend module.',
        [{ text: 'Close', style: 'cancel' }],
        { cancelable: false }
      )
      this.setState({ isImporting: false })
      shouldDisableSetting = false
    } else {
      // Remove last row item.
      const tempList = dataList.slice(0, -1)

      var joinedDetails = []
      var errorList = []

      // Pass this array into insert SKU data function
      const skuLists = []
      // Check duplicate entries (SKU item ID should be unique)
      const skuItemIdList = []

      // Skip first row (header)
      for (i = 1; i < tempList.length; i++) {
        var recordDetails = tempList[i].split(',')

        if (recordDetails.length < TOTAL_COLUMN) {
          // Join with next line
          const lastElement = joinedDetails[joinedDetails.length - 1]
          const firstElement = recordDetails[0]

          // const joinedDetailsRemoveLast = joinedDetails.pop()
          const joinedDetailsRemoveLast = joinedDetails.slice(0, -1)
          // let joinedDetailsRemoveLast = joinedDetails.splice(-1, 1)
          joinedDetails.push(`${lastElement}${firstElement}`)

          let recordDetailsRemoveFirst = joinedDetails.splice(0, 1)
          joinedDetails.push(recordDetailsRemoveFirst)
        } else {
          joinedDetails = recordDetails
        }

        if (joinedDetails.count == TOTAL_COLUMN) {
          recordDetails = joinedDetails
          joinedDetails = []
        } else {
          if (joinedDetails.length > TOTAL_COLUMN) {
            errorList.push(joinedDetails.join())
            joinedDetails = []
            continue
          }
        }

        // new SKU item to be pushed into SKU item list
        const savedSKUitem = []

        // SKU item ID
        var skuItemId = recordDetails[0]
        for (m = 0; m < skuItemId.length; m++) {
          skuItemId = skuItemId.replace('"', '');
        }
        if (skuItemIdList.indexOf(parseInt(skuItemId, 10)) == -1) {
          // Add into SKU item ID list
          skuItemIdList.push(parseInt(skuItemId, 10))
          // Save as SKU information
          savedSKUitem.push(parseInt(skuItemId, 10))
        } else {
          Alert.alert(
            'Error', `The CSV file contains duplicate record.\nSKU Item ID: ${skuItemId}`,
            [{ text: 'Close', style: 'default' }],
            { cancelable: true }
          )
          hasDuplicateEntry = true
          break
        }

        // ARMS Code
        var armsCode = recordDetails[1]
        for (m = 0; m < armsCode.length; m++) {
          armsCode = armsCode.replace('"', '');
        }
        savedSKUitem.push(armsCode)

        // Art No
        savedSKUitem.push(recordDetails[2])

        // MCode
        savedSKUitem.push(recordDetails[3])

        // BarCode
        savedSKUitem.push(recordDetails[4])

        // Product Description
        const prodDescStrSep = recordDetails[5].split('"')
        var prodDescStrList = []
        var prodDescStr
        for (j = 0; j < prodDescStrSep.length; j++) {
          if (prodDescStrSep[j] != '') {
            prodDescStrList.push(prodDescStrSep[j])
          }
        }
        if (prodDescStrList.length == 1) {
          prodDescStr = prodDescStrList[0]
        } else {
          prodDescStr = `"${prodDescStrList[0]}`
          for (k = 1; k < prodDescStrList.length; k++) {
            prodDescStr += `"${prodDescStrList[k]}`
          }
        }
        savedSKUitem.push(prodDescStr)

        // Receipt Description
        savedSKUitem.push(recordDetails[6])

        // Selling Price
        var sellingPrice = recordDetails[7]
        for (m = 0; m < sellingPrice.length; m++) {
          sellingPrice = sellingPrice.replace('"', '');
        }
        savedSKUitem.push(parseFloat(sellingPrice).toFixed(2))

        // Cost Price
        var costPrice = recordDetails[8]
        for (m = 0; m < costPrice.length; m++) {
          costPrice = costPrice.replace('"', '');
        }
        savedSKUitem.push(parseFloat(costPrice).toFixed(2))

        // Price Type
        savedSKUitem.push(recordDetails[9])

        // SKU Type
        savedSKUitem.push(recordDetails[10])

        // Department
        var department = recordDetails[11]
        for (m = 0; m < department.length; m++) {
          department = department.replace('"', '');
        }
        savedSKUitem.push(department)

        // Category
        var category = recordDetails[12]
        for (m = 0; m < category.length; m++) {
          category = category.replace('"', '');
        }
        savedSKUitem.push(category)

        // SKU or PRC
        savedSKUitem.push(recordDetails[13])

        // UOM Code
        savedSKUitem.push(recordDetails[14])

        // Fraction
        savedSKUitem.push(recordDetails[15])

        // Input Tax
        savedSKUitem.push(recordDetails[16])

        // Output Tax
        savedSKUitem.push(recordDetails[17])

        // Inclusive Tax
        savedSKUitem.push((recordDetails[18].indexOf('YES') != -1) ? 1 : 0)

        // Scale Type
        var scaleType = recordDetails[19]
        for (m = 0; m < scaleType.length; m++) {
          scaleType = scaleType.replace('"', '');
        }
        savedSKUitem.push(scaleType)

        // Active
        savedSKUitem.push((recordDetails[20].indexOf('YES') != -1) ? 1 : 0)

        // Brand
        var brand = recordDetails[21]
        for (m = 0; m < brand.length; m++) {
          brand = brand.replace('"', '');
        }
        savedSKUitem.push(brand)

        // Vendor
        var vendor = recordDetails[22]
        for (m = 0; m < vendor.length; m++) {
          vendor = vendor.replace('"', '');
        }
        savedSKUitem.push(vendor)

        // Parent ARMS Code
        savedSKUitem.push(recordDetails[23])

        // Parent Art No
        savedSKUitem.push(recordDetails[24])

        // Parent MCode
        savedSKUitem.push(recordDetails[25])

        skuLists.push(savedSKUitem)
        // count++
      }

      if (!hasDuplicateEntry) {
        Alert.alert(
          'Import in progress', 'This action might take a few minutes.',
          [],
          { cancelable: false }
        )
        sqLite.deleteSKUItems()
        sqLite.insertSKUItems(skuLists)
      }

      this.setState({ isImporting: false });
    }
  }

  _onPress = (rowIndex) => {
    switch (rowIndex) {
      case 0: this.props.navigation.navigate('StockTakeInfo'); break;
      //// Temporary block Price Check for customer testing unit
      // case 1:
      //   // const model = DeviceInfo.getModel()
      //   if (!this.state.priceCheck && IS_NQUIRE_PRICE_CHECKER) {
      //     Alert.alert(
      //       'Important', 'This app will enter full screen mode. Do you want to continue?',
      //       [
      //         {
      //           text: 'Cancel', style: 'cancel'
      //         },
      //         {
      //           text: 'Yes', style: 'default', onPress: () => {
      //             this.setState({ priceCheck: true })
      //             this.props.navigation.navigate('PriceCheck')
      //           }
      //         }
      //       ],
      //       { cancelable: false }
      //     )
      //   } else {
      //     this.props.navigation.navigate('PriceCheck')
      //   }
      //   break
      case 2: this.props.navigation.navigate('ViewReportGenerateReport'); break
      //// Temporary block Import SKU Data for customer testing unit
      // case 3: this.showDocumentPicker(); break
      default: this.featureComingSoon()
    }
  };

  onLayout(e) {
    const { width, height } = Dimensions.get('window')
    this.setState({ screenWidth: width, screenHeight: height })
  }

  render() {
    let landscape = this.state.screenWidth > this.state.screenHeight
    console.log(DeviceInfo.getModel())
    return (
      <View
        onLayout={this.onLayout.bind(this)}
        style={Styles.mainView}
      >
        <View style={[Styles.mainViewHeader, landscape ? { height: 15 } : null]}></View>
        {/* <View style={Styles.mainViewHeader}></View> */}
        <FlatList style={Styles.mainList}
          data={[
            { sourceFile: require('../../Assets/stock_take.png'), key: 'Stock Take Operation' },
            { sourceFile: require('../../Assets/price_label.png'), key: 'Price Check' },
            { sourceFile: require('../../Assets/report.png'), key: 'View Report' },
            { sourceFile: require('../../Assets/file_csv_export.png'), key: 'Import SKU Data' },
            { sourceFile: require('../../Assets/updates.png'), key: 'Upgrade / Activation' },
          ]}
          renderItem={({ item, index, separators }) => (
            <TouchableHighlight
              onPress={() => this._onPress(index)}
              onShowUnderlay={separators.highlight}
              onHideUnderlay={separators.unhighlight}
            >
              <View style={Styles.mainListItem}>
                <Image
                  source={item.sourceFile}
                  style={Styles.mainListImage}
                />
                <Text style={Styles.mainListText}>{item.key}</Text>
              </View>
            </TouchableHighlight>
          )}
        />
        {this.state.isImporting == true ?
          <View style={Styles.mainReadCSVFileView}>
            <Text style={Styles.mainReadCSVText}>Reading file...</Text>
            <ActivityIndicator size="large" color="#157efb" />
          </View>
          : null
        }
      </View>
    );
  }
}