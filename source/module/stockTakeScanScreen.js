import React, { Component } from 'react';
import {
  Platform,
  View,
  ScrollView,
  Text,
  TextInput,
  Image,
  Alert,
  AsyncStorage,
  TouchableOpacity,
  ToastAndroid
} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Styles from '../styles'
import ScannerScreen from './scannerScreen'
import SQLite from './sqlite'

var sqLite = new SQLite();

export default class StockTakeScanScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // txtinput
      stockID: '', quantity: '',
      // scan barcode
      scancode: '', scancodePrev: '',
      // Save button (enable/disable)
      hasFocusTxtInput: false,
      // stock take info list for viewing report
      branchList: [], dateList: [], locationList: [], shelfList: [], userList: [],
      // message after saving Stock Take Record
      newRecordSaved: '', savedRecord: '', savedStockID: '', savedQuantity: '', recordNo: '', count: 0,
    }
  }

  static navigationOptions = ({ navigation, navigationOptions }) => {
    const params = navigation.state.params || {};

    const isQtyOneNav = navigation.getParam('isQtyOne', '')

    return {
      title: 'Stock Take Operation',
      headerRight: (
        <TouchableOpacity onPress={() => navigation.navigate('Scanner',
          {
            isQtyOne: isQtyOneNav,
          })}>
          <View style={Styles.headerButtonPosition}>
            <Image
              source={require('../../Assets/camera.png')}
              style={Styles.headerButton}
            />
          </View>
        </TouchableOpacity>
      ),
    };
  }

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener('didFocus', () => this.firstTextInput.focus());
    this.fetchStockTakeList()

    // load Stock Take records
    var count = 0
    sqLite.open().transaction((tx) => {
      tx.executeSql("select * from stockTakeRecord", [], (tx, results) => {
        var len = results.rows.length;
        for (let i = 0; i < len; i++) {
          var u = results.rows.item(i);
          count++
        }
        this.state.count = count
      });
    }, (error) => {
      console.log(error);
    });
  }

  componentWillUnmount() {
    this.focusListener.remove();
  }

  async fetchStockTakeList() {
    try {
      const branchListString = await AsyncStorage.getItem('@branchList');
      const dateListString = await AsyncStorage.getItem('@dateList');
      const locationListString = await AsyncStorage.getItem('@locationList');
      const shelfListString = await AsyncStorage.getItem('@shelfList');
      const userListString = await AsyncStorage.getItem('@userList');

      const branchListFetch = JSON.parse(branchListString)
      const dateListFetch = JSON.parse(dateListString)
      const locationListFetch = JSON.parse(locationListString)
      const shelfListFetch = JSON.parse(shelfListString)
      const userListFetch = JSON.parse(userListString)

      this.setState({
        branchList: branchListFetch,
        dateList: dateListFetch,
        locationList: locationListFetch,
        shelfList: shelfListFetch,
        userList: userListFetch,
      })

    } catch (error) {
      console.log("Error retrieving data" + error)
    }
  }

  async saveStockTakeList(savedStockTake) {
    var branchListSave = this.state.branchList != null ? this.state.branchList : []
    var dateListSave = this.state.dateList != null ? this.state.dateList : []
    var locationListSave = this.state.locationList != null ? this.state.locationList : []
    var shelfListSave = this.state.shelfList != null ? this.state.shelfList : []
    var userListSave = this.state.userList != null ? this.state.userList : []

    if (branchListSave.indexOf(savedStockTake.branch) == -1) {
      branchListSave.push(savedStockTake.branch)
    }
    if (dateListSave.indexOf(savedStockTake.date) == -1) {
      dateListSave.push(savedStockTake.date)
    }
    if (locationListSave.indexOf(savedStockTake.location) == -1) {
      locationListSave.push(savedStockTake.location)
    }
    if (shelfListSave.indexOf(savedStockTake.shelf) == -1) {
      shelfListSave.push(savedStockTake.shelf)
    }
    if (userListSave.indexOf(savedStockTake.user) == -1) {
      userListSave.push(savedStockTake.user)
    }

    const branchListSavedString = JSON.stringify(branchListSave)
    const dateListSavedString = JSON.stringify(dateListSave)
    const locationListSavedString = JSON.stringify(locationListSave)
    const shelfListSavedString = JSON.stringify(shelfListSave)
    const userListSavedString = JSON.stringify(userListSave)

    try {
      await AsyncStorage.setItem('@branchList', branchListSavedString)
      await AsyncStorage.setItem('@dateList', dateListSavedString)
      await AsyncStorage.setItem('@locationList', locationListSavedString)
      await AsyncStorage.setItem('@shelfList', shelfListSavedString)
      await AsyncStorage.setItem('@userList', userListSavedString)
    } catch (error) {
      console.log("Error saving data" + error)
    }
  }

  isFirstTimeAccess(isQtyOne) {
    if (this.state.hasFocusTxtInput == true) {
      return true
    } else {
      if (isQtyOne) {
        if (this.state.stockID != null && this.state.stockID != '') {
          return false
        } else {
          return true
        }
      } else {
        if (this.state.stockID != null && this.state.stockID != '' &&
          this.state.quantity != null && this.state.quantity != '') {
          return false
        } else {
          return true
        }
      }
    }
  }

  saveRecord(info, id, qty) {
    // create stockTake object
    var stockTake = {
      branch: info[0],
      date: info[1],
      dateSaved: JSON.stringify(new Date()),
      itemCode: id,
      itemQty: qty,
      location: info[2],
      shelf: info[3],
      user: info[4],
    }

    // Save stockTake object into SQLite
    sqLite.insertStockTakeData(stockTake)

    // Update Stock Take list for view report
    this.saveStockTakeList(stockTake)

    // Show message after saving Stock Take Record
    this.state.newRecordSaved = `New Stock Take record saved.`
    this.state.savedRecord = `Stock ID: ${id} | Qty: ${qty}`
    this.state.count++
    this.state.recordNo = `Total no. of Stock Take record(s): ${this.state.count}`

    this.setState({
      stockID: '', quantity: '',
      scancode: '', scancodePrev: '',
    })

    this.props.navigation.setParams({
      qrcode: '', isScanCode: false, shouldAutoSave: false
    })
  }

  _onPress = (isQtyOne, code, stInfoArray) => {
    const { params } = this.props.navigation.state

    // check Scan Qty
    var stringErr = ''
    // convert Scan Qty from String to Int
    var quantityInt
    // stock ID string (scan item will return 'object')
    var stockIDString

    // scan item will return 'object'
    if (typeof this.state.stockID == 'object') {
      stockIDString = code
    } else {
      stockIDString = this.state.stockID
    }

    // set quantityInt and do checking
    if (isQtyOne == true) {
      quantityInt = 1
    } else {
      quantityInt = this.state.quantity.match(/^[0-9]+$/) ?
        parseInt(this.state.quantity, 10) : null
    }
    if (quantityInt != null) {
      if (quantityInt > 0) { stringErr = '' }
      else { stringErr = 'Stock Quantity must be greater than 0.' }
    }
    else {
      stringErr = 'Stock Quantity must contain number 0-9 only.'
    }

    if (stringErr != null && stringErr != '') {
      // show error message
      alert(stringErr)
    } else {
      // Save record into SQLite
      this.saveRecord(stInfoArray, stockIDString, quantityInt)
    }
  }

  render() {
    const { navigation } = this.props;
    const branch = navigation.getParam('branch', '')
    const dateText = navigation.getParam('dateText', '')
    const location = navigation.getParam('location', '')
    const shelf = navigation.getParam('shelf', '')
    const user = navigation.getParam('user', '')
    const isQtyOne = navigation.getParam('isQtyOne', '')

    const stockTakeInfo = [branch, dateText, location, shelf, user]

    const qrcodeTemp = navigation.getParam('qrcode', '')
    const isscancodeTemp = navigation.getParam('isScanCode', false)

    const shouldAutoSave = navigation.getParam('shouldAutoSave', false)

    var savedStockID
    var savedQuantity

    return (
      <View style={Styles.StockTakeInfoView}>
        <ScrollView style={{ width: '100%' }}>
          <View style={[Styles.mainViewHeader, isQtyOne ?
            { height: 55 } : { height: 30 }]}>
            <Text style={[Styles.headerAutosave, isQtyOne ?
              { color: 'black' } : { color: 'transparent' }]}>
              Scanned Stock ID will be autosaved as Stock Take record.
          </Text>
          </View>

          {/* Stock ID */}
          <View style={Styles.StockTakeInfoRow}>
            <Text style={Styles.lblStockTakeInfo}>Stock ID</Text>
            <TextInput
              style={Styles.txtStockTakeInfo}
              autoFocus={true}
              placeholder="Enter stock ID here."
              defaultValue={qrcodeTemp}
              ref={(input) => { this.firstTextInput = input; }}
              onFocus={(value) => {
                var valueText = value
                if (isscancodeTemp) {
                  if (this.state.scancodePrev != qrcodeTemp) {
                    this.state.scancode = this.state.scancodePrev
                    this.state.scancodePrev = qrcodeTemp
                    // Autosave feature
                    if (shouldAutoSave) {
                      valueText = value.nativeEvent.text
                      this.saveRecord(stockTakeInfo, qrcodeTemp, 1)
                    } else {
                      this.state.newRecordSaved = ''
                    }
                  }
                  if (this.state.scancode != this.state.scancodePrev) {
                    this.state.scancode = this.state.scancodePrev
                  } else {
                    this.state.scancode = qrcodeTemp
                  }
                  this.setState({ stockID: valueText, hasFocusTxtInput: true })
                }
              }}
              selectTextOnFocus={true}
              onChangeText={(stockID) => {
                this.setState({ stockID: stockID, hasFocusTxtInput: true })
                this.state.scancode = stockID
                this.state.newRecordSaved = ''
              }}
              onSubmitEditing={(stockID) => {
                if (isQtyOne != true && this.state.quantity == '') {
                  this.secondTextInput.focus();
                  blurOnSubmit = false
                }
                this.setState({ hasFocusTxtInput: false })
              }}
              value={isscancodeTemp ? this.state.scancode : this.state.stockID}
            />
          </View>

          {/* Quantity */}
          <View style={Styles.StockTakeInfoRow}>
            <Text style={Styles.lblStockTakeInfo}>Quantity</Text>
            <TextInput
              editable={isQtyOne ? false : true}
              style={[Styles.txtStockTakeInfo, isQtyOne ?
                { backgroundColor: '#e8e8e8' } : { backgroundColor: '#f8f8f8' }]}
              placeholder="Enter quantity here."
              ref={(input) => { this.secondTextInput = input; }}
              keyboardType='numeric'
              returnKeyType='done'
              onFocus={(quantity) => {
                this.setState({ hasFocusTxtInput: true })
              }}
              onChangeText={(quantity) => {
                this.setState({ quantity: quantity.replace(/[^0-9]/g, ''), hasFocusTxtInput: true, newRecordSaved: '' })
              }}
              onSubmitEditing={(quantity) => {
                this.setState({ hasFocusTxtInput: false })
              }}
              value={isQtyOne ? '1' : this.state.quantity.replace(/[^0-9]/g, '')}
            />
          </View>

          {/* Save */}
          <TouchableOpacity
            disabled={this.isFirstTimeAccess(isQtyOne)}
            onPress={() => {
              this._onPress(isQtyOne, qrcodeTemp, stockTakeInfo)
              this.firstTextInput.focus()
            }}
          >
            <Text
              style={[Styles.btnStockTakeInfo,
              this.isFirstTimeAccess(isQtyOne) ? Styles.btnDisabled : Styles.btnEnabled]}
            >
              Save
          </Text>
          </TouchableOpacity>

          <View style={{ paddingTop: 20 }}>
            <Text style={Styles.saveInfoText}>
              {this.state.newRecordSaved}
            </Text>
            <Text style={Styles.saveInfoText} numberOfLines={1}>
              {this.state.savedRecord}
            </Text>
            <Text style={[Styles.saveInfoText, { marginBottom: 30 }]}>
              {this.state.recordNo}
            </Text>

          </View>
        </ScrollView>
      </View >
    );
  }
}
