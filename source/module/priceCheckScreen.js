import React, { Component } from 'react';
import {
  Platform,
  View,
  Dimensions,
  Text,
  TextInput,
  Alert,
  AsyncStorage,
  Keyboard,
  TouchableOpacity,
  // BackHandler,
  Image,
  StyleSheet,
} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Swiper from 'react-native-swiper'
// import SimpleStepper from 'react-native-simple-stepper'
// import Moment from 'moment'
import Styles from '../styles'
import SQLite from './sqlite'
import FullScreen, { ToggleView } from 'react-native-full-screen'
import DeviceInfo from 'react-native-device-info'

var sqLite = new SQLite();

let IS_NQUIRE_PRICE_CHECKER = (DeviceInfo.getModel() == "NQuire 300")

export default class PriceCheckScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      skuItemList: [],
      showPriceCheckDetails: false, stockID: '',
      armsCode: 'ARMS Code: ', mcode: 'MCode: ', artno: 'Artno: ', barcode: 'Barcode: ',
      prodDesc: '---', uom: '---', fraction: '---', uomFraction: '---', sellingPrice: 'RM 0.00',
      department: 'Department: ', category: 'Category: ', brand: 'Brand: ', vendor: 'Vendor: ',
      skuType: 'SKU Type: ', priceType: 'Price Type: ',
      dbDate: 'Database imported on: ',
      skuRecords: 'Please wait while loading SKU information.',
      itemFound: false,
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
    }
  }

  static navigationOptions = ({ navigation, navigationOptions }) => {
    const params = navigation.state.params || {};
    return {
      title: 'Price Check',
      headerRight:
        !IS_NQUIRE_PRICE_CHECKER ?
          (
            <TouchableOpacity onPress={() => navigation.navigate('PriceCheckScanner')}>
              <View style={Styles.headerButtonPosition}>
                <Image
                  source={require('../../Assets/camera.png')}
                  style={Styles.headerButton}
                />
              </View>
            </TouchableOpacity>
          )
          :
          null,
    };
  };

  componentWillMount() {
    this.fetchSKUitems()
    this.fetchDbImportDate()
  }

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener('didFocus', () => this.textInput.focus());
    // BackHandler.addEventListener('hardwareBackPress', this.handleBackButton)
    if (IS_NQUIRE_PRICE_CHECKER) {
      FullScreen.onFullScreen();
    }
  }

  componentWillUnmount() {
    this.focusListener.remove();
    // BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton)
  }

  onLayout(e) {
    const { width, height } = Dimensions.get('window')
    this.setState({ screenWidth: width, screenHeight: height })
  }

  fetchSKUitems() {
    sqLite.open().transaction((tx) => {
      tx.executeSql("select * from SKUItem", [], (tx, results) => {
        var len = results.rows.length;
        if (len != 0) {
          for (let i = 0; i < len; i++) {
            var item = results.rows.item(i);
            this.state.skuItemList.push(item)
          }
          this.setState({ skuRecords: `No of SKU records: ${len}` })
        } else {
          Alert.alert(
            'Error', `Please import SKU data before using Price Check module`,
            [{ text: 'OK', onPress: () => this.props.navigation.goBack() }],
            { cancelable: false }
          )
        }
      });
    }, (error) => {
      console.log(error);
    });
  }

  async fetchDbImportDate() {
    try {
      const getDbImportDate = await AsyncStorage.getItem('@dbDate');
      this.setState({ dbDate: `Database imported on: ${getDbImportDate}` })
    } catch (error) {
      console.log("Error retrieving data" + error)
    }
  }

  searchProduct(scanCode) {
    const { navigation } = this.props;
    const qrcodeTemp = navigation.getParam('qrcode', '')
    const list = this.state.skuItemList

    // scanCodeTemp is displayed when item not found
    var scanCodeTemp = scanCode

    if (scanCode != '') {
      if (this.swiper != null) {
        this.swipe(0)
      }

      this.setState({ showPriceCheckDetails: true })

      for (i = 0; i < list.length; i++) {
        if (scanCode == list[i].armsCode || scanCode.toLowerCase() == list[i].artno.toLowerCase() ||
          scanCode == list[i].barcode || scanCode == list[i].mcode) {
          this.setState({
            armsCode: `ARMS Code: ${list[i].armsCode}`,
            mcode: `MCode: ${(list[i].mcode != '') ? list[i].mcode : '-'}`,
            artno: `Artno: ${(list[i].artno != '') ? list[i].artno : '-'}`,
            barcode: `Barcode: ${(list[i].barcode != '') ? list[i].barcode : '-'}`,
            prodDesc: `${list[i].productDescription}`,
            uom: `UOM: ${list[i].uomCode}`,
            fraction: `(${list[i].fraction})`,
            uomFraction: `(${list[i].uomCode}, ${list[i].fraction})`,
            sellingPrice: `RM ${parseFloat(list[i].sellingPrice).toFixed(2)}`,
            department: `Department: ${(list[i].department != '') ? list[i].department : '-'}`,
            category: `Category: ${(list[i].category != '') ? list[i].category : '-'}`,
            brand: `Brand: ${(list[i].brand != '') ? list[i].brand : '-'}`,
            vendor: `Vendor: ${(list[i].vendor != '') ? list[i].vendor : '-'}`,
            skuType: `SKU Type: ${list[i].skuType}`,
            priceType: (list[i].skuType.indexOf('CONSIGN') != -1) ? `Price Type: ${list[i].priceType}` : '',
            itemFound: true,
          })
          break
        } else if (i == list.length - 1) {
          if (scanCode.length == 13) {
            // truncate the scanned code and reset the for-loop
            scanCode = scanCode.substring(0, 12)
            i = -1
          } else {
            this.setState({
              armsCode: `ARMS Code: -`,
              mcode: `MCode: -`,
              artno: `Artno: -`,
              barcode: `Barcode: -`,
              prodDesc: `Scanned Code: ${scanCodeTemp}`,
              uom: `UOM: -`,
              fraction: ``,
              uomFraction: ``,
              sellingPrice: `Item not found.`,
              department: `Department: -`,
              category: `Category: -`,
              brand: `Brand: -`,
              vendor: `Vendor: -`,
              skuType: `SKU Type: -`,
              priceType: ``,
              itemFound: false,
            })
          }
        }
      }
      // Clear param "qrcode"
      if (qrcodeTemp != '') {
        this.setState({ stockID: qrcodeTemp })
        navigation.setParams({ qrcode: '' })
      }
    }
  }

  // handleBackButton() {
  //   console.log('Hardware back button is disabled.')
  //   return true
  // }

  // swipe back to the first slide: show price/item not found.
  swipe(targetIndex) {
    const currentIndex = this.swiper.state.index;
    const offset = targetIndex - currentIndex;
    this.swiper.scrollBy(offset);
  }

  ShowAlertWithDelay = () => {
    // set time delay for scanning new item code.
    if (IS_NQUIRE_PRICE_CHECKER) {
      setTimeout(() => {
        if (this.textInput != null && !this.textInput.isFocused()) {
          // this.textInput.clear()
          this.textInput.focus()
        }
        // 5000 milliseconds
      }, 5000);
    }

  }

  render() {
    const { navigation } = this.props;
    const qrcodeTemp = navigation.getParam('qrcode', '')

    var portrait = this.state.screenWidth < this.state.screenHeight

    return (

      <View
        onLayout={this.onLayout.bind(this)}
        style={Styles.priceCheckView}
      >
        {portrait ? <View style={Styles.mainViewHeader}></View> : null}

        {/* Scan Product */}
        <View style={Styles.scanProductView}>
          <Text style={Styles.lblPriceCheckScanProduct}>
            Scan Product
          </Text>
          <TextInput
            style={Styles.txtPriceCheckScanProduct}
            placeholder="Scan product here."
            ref={(input) => { this.textInput = input; }}
            disableFullscreenUI={true}
            onFocus={(value) => {
              if (qrcodeTemp != '') {
                this.searchProduct(qrcodeTemp)
                Keyboard.dismiss()
              }
            }}
            onChangeText={(code) => {
              this.setState({ stockID: code })
            }}
            onSubmitEditing={(code) => {
              this.searchProduct(code.nativeEvent.text)
              this.ShowAlertWithDelay()
            }}
            value={qrcodeTemp != '' ? qrcodeTemp : this.state.stockID}
          />
        </View>

        {/* Price Check Details */}
        {this.state.showPriceCheckDetails ? (
          portrait ?
            /*** A. Price Check View in Portrait mode STRATS ***/
            <View style={Styles.priceCheckDetailView}>

              {/* ARMS Code */}
              <View style={Styles.priceCheckRow}>
                <Text style={Styles.lblPriceCheck} numberOfLines={1}>{this.state.armsCode}</Text>
              </View>

              {/* MCode */}
              <View style={Styles.priceCheckRow}>
                <Text style={Styles.lblPriceCheck} numberOfLines={1}>{this.state.mcode}</Text>
              </View>

              {/* Artno */}
              <View style={Styles.priceCheckRow}>
                <Text style={Styles.lblPriceCheck} numberOfLines={1}>{this.state.artno}</Text>
              </View>

              {/* Barcode */}
              <View style={Styles.priceCheckRow}>
                <Text style={Styles.lblPriceCheck} numberOfLines={1}>{this.state.barcode}</Text>
              </View>

              {/* Product Description */}
              <View style={Styles.priceCheckRow}>
                <Text style={[Styles.lblPriceCheck, { fontWeight: 'bold' }]} numberOfLines={1}>{this.state.prodDesc}</Text>
              </View>

              {/* UOM (Fraction) */}
              <View style={Styles.priceCheckRow}>
                <Text style={Styles.lblPriceCheck} numberOfLines={1}>{this.state.uom} {this.state.fraction}</Text>
              </View>

              {/* Selling Price */}
              <View style={Styles.sellingPriceRow}>
                {this.state.itemFound ?
                  <Text style={Styles.lblSellingPrice} numberOfLines={1}>{this.state.sellingPrice}</Text> :
                  <Text style={Styles.lblPriceNotFound}>{this.state.sellingPrice}</Text>
                }
              </View>

              {/* Department */}
              <View style={Styles.priceCheckRow}>
                <Text style={Styles.lblPriceCheck} numberOfLines={1}>{this.state.department}</Text>
              </View>

              {/* Category */}
              <View style={Styles.priceCheckRow}>
                <Text style={Styles.lblPriceCheck} numberOfLines={1}>{this.state.category}</Text>
              </View>

              {/* Brand */}
              <View style={Styles.priceCheckRow}>
                <Text style={Styles.lblPriceCheck} numberOfLines={1}>{this.state.brand}</Text>
              </View>

              {/* Vendor */}
              <View style={Styles.priceCheckRow}>
                <Text style={Styles.lblPriceCheck} numberOfLines={1}>{this.state.vendor}</Text>
              </View>

              {/* SKU Type & Price Type */}
              <View style={Styles.priceCheckRow}>
                <Text style={Styles.lblPriceCheck}>{this.state.skuType}</Text>
                <Text style={Styles.lblPriceCheck}>{this.state.priceType}</Text>
              </View>

            </View>
            /*** A. Price Check View in Portrait mode ENDS ***/
            :
            /*** B. Price Check View in Landscape mode STARTS ***/
            (
              IS_NQUIRE_PRICE_CHECKER ?

                /** B1. Price Check for Newland NQuire Price Checker STARTS **/
                <View style={Styles.priceCheckDetailView}>

                  {/* Product Description (UOM, Fraction) */}
                  <View style={Styles.priceCheckRowLandscape}>
                    <Text style={Styles.lblPriceCheckProdLandscape} numberOfLines={1}>{this.state.prodDesc} </Text>
                    <Text style={[Styles.lblPriceCheckProdLandscape, { color: 'red' }]}>{this.state.uomFraction}</Text>
                  </View>

                  <View style={{ height: 80 }}>

                    {/*** Swiper view ***/}
                    <Swiper horizontal={false} loop={false} ref={component => this.swiper = component}>

                      {/* Swiper View First Slide */}
                      <View style={Styles.slider}>
                        {/* Selling Price */}
                        <View style={Styles.sellingPriceRow}>
                          {this.state.itemFound ?
                            <Text style={Styles.lblSellingPrice} numberOfLines={1}>{this.state.sellingPrice}</Text>
                            :
                            <Text style={Styles.lblPriceNotFound}>{this.state.sellingPrice}</Text>
                          }
                        </View>
                      </View>

                      {/* Swiper View Second Slide */}
                      <View style={Styles.slider}>
                        {/* ARMS Code */}
                        <View style={Styles.priceCheckRowLandscape}>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.armsCode}</Text>
                        </View>
                        {/* Mcode */}
                        <View style={Styles.priceCheckRowLandscape}>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.mcode}</Text>
                        </View>
                        {/* Artno */}
                        <View style={Styles.priceCheckRowLandscape}>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.artno}</Text>
                        </View>
                        {/* Barcode */}
                        <View style={Styles.priceCheckRowLandscape}>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.barcode}</Text>
                        </View>
                      </View>

                      {/** Swiper View Third Slide **/}
                      <View style={Styles.slider}>
                        {/* Department */}
                        <View style={Styles.priceCheckRowLandscape}>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.department}</Text>
                        </View>
                        {/* Category */}
                        <View style={Styles.priceCheckRowLandscape}>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.category}</Text>
                        </View>
                        {/* Brand */}
                        <View style={Styles.priceCheckRowLandscape}>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.brand}</Text>
                        </View>
                        {/* Vendor */}
                        <View style={Styles.priceCheckRowLandscape}>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.vendor}</Text>
                        </View>
                        {/* SKU Type & Price Type */}
                        <View style={Styles.priceCheckRowLandscape}>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.skuType}</Text>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.priceType}</Text>
                        </View>
                      </View>

                    </Swiper>
                  </View>

                </View>
                /** B1. Price Check for Newland NQuire Price Checker ENDS **/
                :
                /** B2. Price Check for other devices (Landscape) STARTS **/
                <View style={Styles.priceCheckDetailView}>

                  {/* Product Description */}
                  <View style={Styles.priceCheckRowLandscape}>
                    <Text style={[Styles.lblPriceCheckProdLandscape, { fontWeight: 'bold' }]}>{this.state.prodDesc}</Text>
                  </View>

                  {/* Selling Price */}
                  <View style={[Styles.sellingPriceRow, { height: 80 }]}>
                    {this.state.itemFound ?
                      <Text style={[Styles.lblSellingPrice, { paddingVertical: 8 }]}>{this.state.sellingPrice}</Text>
                      :
                      <Text style={[Styles.lblPriceNotFound, { paddingVertical: 15 }]}>{this.state.sellingPrice}</Text>
                    }
                  </View>

                  {/*** Swiper view ***/}
                  <View style={{ height: 80 }}>
                    <Swiper horizontal={false} loop={false} ref={component => this.swiper = component}>

                      {/** Swiper View First Slide **/}
                      <View style={Styles.slider}>
                        {/* ARMS Code, Mcode */}
                        <View style={Styles.priceCheckRowLandscape}>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.armsCode}</Text>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.mcode}</Text>
                        </View>
                        {/* Barcode, Artno */}
                        <View style={Styles.priceCheckRowLandscape}>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.barcode}</Text>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.artno}</Text>
                        </View>
                        {/* UOM */}
                        <View style={Styles.priceCheckRowLandscape}>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.uom} {this.state.fraction}</Text>
                        </View>
                      </View>

                      {/** Swiper View Second Slide **/}
                      <View style={Styles.slider}>
                        {/* Department */}
                        <View style={Styles.priceCheckRowLandscape}>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.department}</Text>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.category}</Text>
                        </View>
                        {/* Brand */}
                        <View style={Styles.priceCheckRowLandscape}>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.brand}</Text>
                        </View>
                        {/* Vendor */}
                        <View style={Styles.priceCheckRowLandscape}>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.vendor}</Text>
                        </View>
                        {/* SKU Type & Price Type */}
                        <View style={Styles.priceCheckRowLandscape}>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.skuType}</Text>
                          <Text style={Styles.lblPriceCheckLandscape} numberOfLines={1}>{this.state.priceType}</Text>
                        </View>
                      </View>

                    </Swiper>
                  </View>

                </View>

              /** B2. Price Check for other devices (Landscape) ENDS **/
            )
          /*** B. Price Check View in Landscape mode ENDS ***/
        )
          :
          null
        }

        {/* DB Date & SKU Records */}
        {portrait ?
          /*** Portrait mode ***/
          <View style={Styles.databaseRow}>
            <Text style={Styles.lblDatabase}>{this.state.dbDate}</Text>
            <Text style={Styles.lblDatabase}>{this.state.skuRecords}</Text>
          </View>
          :
          /*** Landscape mode ***/
          <View>
            {this.state.showPriceCheckDetails ?
              <Text style={Styles.lblDatabase}>Swipe left/right for more information.</Text> : null
            }
            <Text style={Styles.lblDatabase}>{this.state.dbDate} | {this.state.skuRecords}</Text>
          </View>
        }

      </View >
    );
  }
}
