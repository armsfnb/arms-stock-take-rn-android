import React, { Component } from 'react';
import {
  Platform,
  View,
  Text,
  Alert,
  FlatList,
  Dimensions,
  Switch,
  AsyncStorage,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Styles from '../styles'
import Moment from 'moment'
import SQLite from './sqlite'

var sqLite = new SQLite();

export default class SettingsClearDataSelectCriteriaScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stateData: [],
      newData: [],
      criteria: '',
      stockTakeRecordList: [],
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
    }
    this.fetchStockTakeInfoList();
  }

  static navigationOptions = ({ navigation, navigationOptions }) => {
    const params = navigation.state.params || {};
    return {
      title: params ? `Select ${params.criteria}` : 'Select Criteria',
    };
  }

  componentDidMount() {
  }

  componentWillUnmount() {
  }

  async fetchStockTakeInfoList() {
    const { params } = this.props.navigation.state
    var stateDataList

    var fetchKey
    switch (params.criteria) {
      case 'Branch': fetchKey = '@branchList'; break
      case 'Date': fetchKey = '@dateList'; break
      case 'Location': fetchKey = '@locationList'; break
      case 'Shelf': fetchKey = '@shelfList'; break
      case 'User': fetchKey = '@userList'; break
      default: break
    }
    try {
      const stateDataString = await AsyncStorage.getItem(fetchKey);
      stateDataList = stateDataString != null ? JSON.parse(stateDataString) : []

      if (params.criteria == 'Date') {
        stateDataList = this.sortDate(stateDataList)
      } else {
        stateDataList.sort();
      }

      if (stateDataList.length != 0) {
        this.state.stateData = stateDataList
      }

    } catch (error) {
      console.log("Error retrieving data" + error)
    }

    var len
    if (this.state.stateData != null) {
      len = this.state.stateData.length
    } else {
      len = 0
    }

    var newDataList = []

    for (let i = 0; i < len; i++) {
      newDataList.push({ key: this.state.stateData[i] })
    }

    this.setState({
      newData: newDataList
    })
  }

  sortDate(dateList) {
    var dateListTemp = dateList
    let dateFormat = 'DD/MM/YYYY'
    let dateFormatSort = 'YYYYMMDD'

    for (let i = 0; i < dateListTemp.length; i++) {
      var momentObj = Moment(dateListTemp[i], dateFormat)
      dateListTemp[i] = momentObj.format(dateFormatSort)
    }

    // Sort date in descending order: year, month, day.
    dateListTemp.sort((num1, num2) => num2 - num1);
    // given (num1, num2) => num1 - num2: ascending;
    // if num1 - num2 < 0: remain num1 and num2
    // if num1 - num2 > 0: swap num1 and num2

    // given (num1, num2) => num2 - num1: descending;
    // if num2 - num1 < 0: remain num1 and num2
    // if num2 - num1 > 0: swap num1 and num2

    // dateListTemp.sort((num1, num2) => num1 > num2 ? -1 : 1);

    for (let i = 0; i < dateList.length; i++) {
      var momentObj = Moment(dateListTemp[i], dateFormatSort)
      dateListTemp[i] = momentObj.format(dateFormat)
    }

    return dateListTemp
  }

  clearRecordsFromCriteria(rowIndex) {
    var query = 'delete from stockTakeRecord where '

    switch (this.state.criteria) {
      case 'Branch': query += 'branch'; break
      case 'Date': query += 'dateStockTake'; break
      case 'Location': query += 'location'; break
      case 'Shelf': query += 'shelf'; break
      case 'User': query += 'user'; break
    }

    sqLite.open().transaction((tx) => {
      tx.executeSql(`${query} = '${this.state.stateData[rowIndex]}'`, [], (tx, results) => {
        this.removeStockTakeList()
      });
    }, (error) => { console.log(error); });
  }

  generateStockTakeList() {
    sqLite.open().transaction((tx) => {
      tx.executeSql(`select * from stockTakeRecord`, [], (tx, results) => {
        var len = results.rows.length;
        for (let i = 0; i < len; i++) {
          var u = results.rows.item(i);
          this.state.stockTakeRecordList.push(u)
        }

        Alert.alert(
          'Clear Records', `Stock Take records for selected ${this.state.criteria.toLowerCase()} have been cleared.`,
          [{
            text: 'OK', onPress: () => {

              if (this.state.stockTakeRecordList.length > 0) {
                this.props.navigation.goBack()
                this.saveStockTakeList()
              } else {
                Alert.alert(
                  'No Stock Take record',
                  'Database is now empty.',
                  [{ text: 'Close', style: 'default', onPress: () => this.props.navigation.navigate('SettingsMain') }],
                  { cancelable: false }
                )
              }

            }
          }],
          { cancelable: false }
        )
      });
    }, (error) => {
      console.log(error);
    });
  }

  async removeStockTakeList() {
    this.generateStockTakeList()
    try {
      await AsyncStorage.removeItem('@branchList')
      await AsyncStorage.removeItem('@dateList')
      await AsyncStorage.removeItem('@locationList')
      await AsyncStorage.removeItem('@shelfList')
      await AsyncStorage.removeItem('@userList')

    } catch (error) {
      console.log("Error removing data" + error)
    }
  }

  async saveStockTakeList() {
    const newBranchList = []
    const newDateList = []
    const newLocationList = []
    const newShelfList = []
    const newUserList = []

    for (let i = 0; i < this.state.stockTakeRecordList.length; i++) {
      var u = this.state.stockTakeRecordList[i];

      if (newBranchList.indexOf(u.branch) == -1) {
        newBranchList.push(u.branch)
      }
      if (newDateList.indexOf(u.dateStockTake) == -1) {
        newDateList.push(u.dateStockTake)
      }
      if (newLocationList.indexOf(u.location) == -1) {
        newLocationList.push(u.location)
      }
      if (newShelfList.indexOf(u.shelf) == -1) {
        newShelfList.push(u.shelf)
      }
      if (newUserList.indexOf(u.user) == -1) {
        newUserList.push(u.user)
      }
    }
    const newBranchListString = JSON.stringify(newBranchList)
    const newDateListString = JSON.stringify(newDateList)
    const newLocationListString = JSON.stringify(newLocationList)
    const newShelfListString = JSON.stringify(newShelfList)
    const newUserListString = JSON.stringify(newUserList)

    try {
      await AsyncStorage.setItem('@branchList', newBranchListString)
      await AsyncStorage.setItem('@dateList', newDateListString)
      await AsyncStorage.setItem('@locationList', newLocationListString)
      await AsyncStorage.setItem('@shelfList', newShelfListString)
      await AsyncStorage.setItem('@userList', newUserListString)
    } catch (error) {
      console.log("Error saving data" + error)
    }
  }

  _onPress = (index) => {
    Alert.alert(
      'Are you sure?', 'This action cannot be undone.',
      [
        { text: 'Cancel', style: 'cancel' },
        { text: 'Clear', style: 'destructive', onPress: () => this.clearRecordsFromCriteria(index) }
      ],
      { cancelable: false }
    )
    // alert(index)
  };

  onLayout(e) {
    const { width, height } = Dimensions.get('window')
    this.setState({ screenWidth: width, screenHeight: height })
  }

  render() {
    const { navigation } = this.props;
    const criteria = navigation.getParam('criteria', '')
    this.state.criteria = criteria

    let landscape = this.state.screenWidth > this.state.screenHeight

    return (
      <View
        onLayout={this.onLayout.bind(this)}
        style={Styles.mainView}
      >
        <View style={[Styles.mainViewHeader, landscape ? { height: 15 } : null]}></View>
        <FlatList style={Styles.mainList}
          data={this.state.newData}
          renderItem={({ item, index, separators }) => (
            <TouchableHighlight
              onPress={() => this._onPress(index)}
              onShowUnderlay={separators.highlight}
              onHideUnderlay={separators.unhighlight}
            >
              <View style={Styles.mainListItem}>
                <Text style={Styles.mainListText}>
                  {item.key}
                </Text>
              </View>
            </TouchableHighlight>
          )}
        />
      </View>
    )
  }
}
