/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  Text,
  View,
  Image,
  Alert,
  FlatList,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Styles from '../styles'
import SQLite from './sqlite'

var sqLite = new SQLite();

export default class SettingsMainScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
    }
  }

  static navigationOptions = ({ navigation, navigationOptions }) => {
    const params = navigation.state.params || {};

    return {
      title: 'Settings',
    };
  };

  componentWillMount() {
    sqLite.open().transaction((tx) => {
      tx.executeSql("select itemCode from stockTakeRecord limit 1", [], (tx, results) => {
        this.state.count = results.rows.length
      });
    }, (error) => {
      console.log(error);
    });
  }

  componentDidMount() {
    this.props.navigation.setParams({ handleAlert: this.featureComingSoon });
  }

  featureComingSoon = () => {
    Alert.alert(
      'Coming Soon',
      'This feature will be available soon.',
      [{ text: 'Close', style: 'cancel' }],
      { cancelable: false }
    )
  }

  _onPress = (rowIndex) => {
    switch (rowIndex) {
      case 0:
        this.props.navigation.navigate('SettingsExportFileFormatSetup')
        break
      case 1:
        // this.props.navigation.navigate('SettingsClearData')
        if (this.state.count > 0) {
          this.props.navigation.navigate('SettingsClearData')
        } else {
          Alert.alert(
            'No Stock Take Record',
            'Database is now empty.',
            [{ text: 'Close', style: 'cancel' }],
            { cancelable: false }
          )
        }
        break
      default:
        this.featureComingSoon()
    }
  };

  onLayout(e) {
    const { width, height } = Dimensions.get('window')
    this.setState({ screenWidth: width, screenHeight: height })
  }

  render() {
    let landscape = this.state.screenWidth > this.state.screenHeight

    return (
      <View
        onLayout={this.onLayout.bind(this)}
        style={Styles.mainView}
      >
        {/* <View style={Styles.mainViewHeader}></View>
       */}
        <View style={[Styles.mainViewHeader, landscape ? { height: 15 } : null]}></View>
        <FlatList style={Styles.mainList}
          data={[
            { sourceFile: require('../../Assets/file_csv_import.png'), key: 'Export File Format Setup' },
            { sourceFile: require('../../Assets/trash_can.png'), key: 'Clear Stock Take Records' },
          ]}
          renderItem={({ item, index, separators }) => (
            <TouchableHighlight
              onPress={() => this._onPress(index)}
              onShowUnderlay={separators.highlight}
              onHideUnderlay={separators.unhighlight}
            >
              <View style={Styles.mainListItem}>
                <Image
                  source={item.sourceFile}
                  style={Styles.mainListImage}
                />
                <Text style={Styles.mainListText}>
                  {item.key}
                </Text>
              </View>
            </TouchableHighlight>
          )}
        />
      </View>
    );
  }
}