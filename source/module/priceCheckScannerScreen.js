import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableOpacity
} from 'react-native'
import { NavigationActions } from 'react-navigation';
import Camera from 'react-native-camera'
import Styles from '../styles'
import StockTakeScanScreen from './stockTakeScanScreen'

export default class BarcodeScan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qrcode: ''
    }
  }

  onBarCodeRead = (e) => {
    // onBarCodeRead = (e, isQtyOne) => {
    // const shouldAutoSave = isQtyOne ? true : false
    this.props.navigation.navigate('PriceCheck', {
      qrcode: e,
    //   isScanCode: true,
    //   shouldAutoSave: shouldAutoSave,
    }, )
  }

  render() {
    const { navigation } = this.props;
    // const isQtyOne = navigation.getParam('isQtyOne', false)

    return (
      <View style={Styles.scannerContainer}>
        <Camera
          style={Styles.scannerPreview}
          onBarCodeRead={(e) => this.onBarCodeRead(e.data)}
          ref={cam => this.camera = cam}
          aspect={Camera.constants.Aspect.fill}
        >
          <Text style={{ backgroundColor: 'white' }}>{this.state.qrcode}</Text>
        </Camera>

        {/* Header */}
        <View style={[Styles.scannerHeaderFooter, Styles.headerView]}>
          {/* Scan title and all those items... */}
          <Text style={Styles.headerText}>Scan</Text>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={Styles.scannerReturn}
          >
            <Image
              source={require('../../Assets/cross.png')}
              style={Styles.scannerHeaderCross}
            />
          </TouchableOpacity>
        </View>

        {/* Footer */}
        <View style={[Styles.scannerHeaderFooter, Styles.footerView]}>
          <Text style={Styles.footerText}>Point your camera at the code so it sits at the center of the screen.</Text>
        </View>

      </View>
    )
  }
}
