import React, { Component } from 'react';
import {
  Platform,
  View,
  ScrollView,
  Text,
  TextInput,
  Switch,
  Alert,
  AsyncStorage,
  TouchableOpacity,
} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import { DatePickerDialog } from 'react-native-datepicker-dialog';
import Moment from 'moment'
import Styles from '../styles'

export default class StockTakeInfoScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      branch: '',
      dateText: Moment(new Date()).format('DD/MM/YYYY'),
      location: '',
      shelf: '',
      user: '',
      isQtyOne: false,
      dateHolder: null,
    }
  }

  static navigationOptions = ({ navigation, navigationOptions }) => {
    const params = navigation.state.params || {};
    return {
      title: 'Stock Take Operation',
    };
  };

  componentDidMount() {
    this.fetchStockTakeInfo()
    this.isFirstTimeAccess()
    this.focusListener = this.props.navigation.addListener('didFocus', () => {
      if (this.state.branch == '' || this.state.branch == null) {
        this.branchTextInput.focus()
      } else if (this.state.location == '' || this.state.location == null) {
        this.locationTextInput.focus()
      } else if (this.state.shelf == '' || this.state.shelf == null) {
        this.shelfTextInput.focus()
      } else if (this.state.user == '' || this.state.user == null) {
        this.userTextInput.focus()
      }
    });
  }

  componentWillUnmount() {
    this.focusListener.remove();
  }

  async fetchStockTakeInfo() {
    try {
      const getBranch = await AsyncStorage.getItem('@stockTakeBranch');
      const getLocation = await AsyncStorage.getItem('@stockTakeLocation');
      const getShelf = await AsyncStorage.getItem('@stockTakeShelf');
      const getUser = await AsyncStorage.getItem('@stockTakeUser');
      const getIsQtyOne = await AsyncStorage.getItem('@stockTakeIsQtyOne');
      var boolIsQtyOne
      if (getIsQtyOne == null || getIsQtyOne == 'NO') {
        boolIsQtyOne = false
      } else if (getIsQtyOne == 'YES') {
        boolIsQtyOne = true
      }
      this.setState({
        branch: getBranch,
        location: getLocation,
        shelf: getShelf,
        user: getUser,
        isQtyOne: boolIsQtyOne
      })
    } catch (error) {
      console.log("Error retrieving data" + error)
    }
  }

  async saveStockTakeInfo(value, setting) {
    var key, savedValue
    switch (setting) {
      case 'branch':
        key = '@stockTakeBranch'; savedValue = value; break
      case 'location':
        key = '@stockTakeLocation'; savedValue = value; break
      case 'shelf':
        key = '@stockTakeShelf'; savedValue = value; break
      case 'user':
        key = '@stockTakeUser'; savedValue = value; break
      case 'isQtyOne':
        key = '@stockTakeIsQtyOne'
        if (value == true) {
          savedValue = 'NO'
        } else {
          savedValue = 'YES'
        }
        break
    }
    try {
      await AsyncStorage.setItem(key, savedValue);
    } catch (error) {
      console.log("Error saving data" + error)
    }
  }

  datePickerMainFunctionCall = () => {
    let dateHolder = this.state.dateHolder

    if (!dateHolder || dateHolder == null) {
      dateHolder = new Date();
      this.setState({
        dateHolder: dateHolder
      })
    }
    this.refs.DatePickerDialog.open({
      date: dateHolder,
      maxDate: new Date()
    })
  }

  onDatePickedFunction = (date) => {
    this.setState({
      dobDate: date,
      dateText: Moment(date).format('DD/MM/YYYY')
    })
  }

  changeSwitchValue = (value) => {
    return (
      this.setState({
        isQtyOne: !this.state.isQtyOne
      }),
      this.saveStockTakeInfo(this.state.isQtyOne, 'isQtyOne')
    )
  }

  isFirstTimeAccess() {
    if (this.state.branch != null && this.state.branch != '' &&
      this.state.location != null && this.state.location != '' &&
      this.state.shelf != null && this.state.shelf != '' &&
      this.state.user != null && this.state.user != '') {
      return false
    } else {
      return true
    }
  }

  _onPress = () => {
    this.props.navigation.navigate('StockTakeScan', {
      branch: this.state.branch,
      dateText: this.state.dateText,
      location: this.state.location,
      shelf: this.state.shelf,
      user: this.state.user,
      isQtyOne: this.state.isQtyOne,
    })
  };

  render() {
    return (
      <View style={Styles.StockTakeInfoView}>
        <ScrollView style={{ width: '100%' }}>

          <View style={Styles.mainViewHeader}></View>

          {/* Branch */}
          <View style={Styles.StockTakeInfoRow}>
            <Text style={Styles.lblStockTakeInfo}>Branch</Text>
            <TextInput
              style={Styles.txtStockTakeInfo}
              placeholder="Enter branch here."
              ref={(input) => { this.branchTextInput = input; }}
              onChangeText={(branch) => {
                this.setState({ branch })
                this.saveStockTakeInfo(branch, 'branch')
              }}
              onSubmitEditing={() => {
                if (this.state.location == '') {
                  this.locationTextInput.focus();
                  blurOnSubmit = false
                }
              }}
              value={this.state.branch}
            />
          </View>

          {/* Select Date */}
          <View style={Styles.StockTakeInfoRow}>
            <Text style={Styles.lblStockTakeInfo}>Date</Text>
            <TouchableOpacity
              style={Styles.btnStockTakeDate}
              onPress={this.datePickerMainFunctionCall.bind(this)}
            >
              <Text style={Styles.strStockTakeDate}>{this.state.dateText}</Text>
            </TouchableOpacity>
            <DatePickerDialog
              ref="DatePickerDialog"
              onDatePicked={this.onDatePickedFunction.bind(this)}
            />
          </View>

          {/* Location */}
          <View style={Styles.StockTakeInfoRow}>
            <Text style={Styles.lblStockTakeInfo}>Location</Text>
            <TextInput
              style={Styles.txtStockTakeInfo}
              placeholder="Enter location here."
              ref={(input) => { this.locationTextInput = input; }}
              onChangeText={(location) => {
                this.setState({ location })
                this.saveStockTakeInfo(location, 'location')
              }}
              onSubmitEditing={() => {
                if (this.state.shelf == '') {
                  this.shelfTextInput.focus();
                  blurOnSubmit = false
                }
              }}
              value={this.state.location}
            />
          </View>

          {/* Shelf */}
          <View style={Styles.StockTakeInfoRow}>
            <Text style={Styles.lblStockTakeInfo}>Shelf</Text>
            <TextInput
              style={Styles.txtStockTakeInfo}
              placeholder="Enter shelf here."
              ref={(input) => { this.shelfTextInput = input; }}
              onChangeText={(shelf) => {
                this.setState({ shelf })
                this.saveStockTakeInfo(shelf, 'shelf')
              }}
              onSubmitEditing={() => {
                if (this.state.user == '') {
                  this.userTextInput.focus();
                  blurOnSubmit = false
                }
              }}
              value={this.state.shelf}
            />
          </View>

          {/* User */}
          <View style={Styles.StockTakeInfoRow}>
            <Text style={Styles.lblStockTakeInfo}>User</Text>
            <TextInput
              style={Styles.txtStockTakeInfo}
              ref={(input) => { this.userTextInput = input; }}
              placeholder="Enter user here."
              onChangeText={(user) => {
                this.setState({ user })
                this.saveStockTakeInfo(user, 'user')
              }}
              value={this.state.user}
            />
          </View>

          {/* Scan Item with default qty = 1 */}
          <View style={Styles.StockTakeInfoRow}>
            <Text style={[Styles.lblStockTakeInfo, { width: 255 }]}>
              Scan item with default qty = 1
          </Text>
            <Switch
              style={Styles.switchIsQtyOne}
              onValueChange={() => this.changeSwitchValue()}
              value={this.state.isQtyOne}
            />
          </View>

          {/* Confirm */}
          <TouchableOpacity
            disabled={this.isFirstTimeAccess()}
            onPress={() => this._onPress()}
          >
            <Text
              style={[Styles.btnStockTakeInfo,
              this.isFirstTimeAccess() ? Styles.btnDisabled : Styles.btnEnabled]}
            >
              Confirm
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}