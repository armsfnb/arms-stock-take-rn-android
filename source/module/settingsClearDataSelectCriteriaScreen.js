import React, { Component } from 'react';
import {
  Platform,
  View,
  Text,
  Alert,
  FlatList,
  Dimensions,
  Switch,
  AsyncStorage,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Styles from '../styles'

export default class SettingsClearDataSelectCriteriaScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filterBy: 'Branch',
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
    }
  }

  static navigationOptions = ({ navigation, navigationOptions }) => {
    const params = navigation.state.params || {};
    return {
      title: 'Filter by',
    };
  }

  componentDidMount() {
  }

  componentWillUnmount() {
  }

  _onPress = (index) => {
    switch (index) {
      case 0: this.state.filterBy = 'Branch'; break
      case 1: this.state.filterBy = 'Date'; break
      case 2: this.state.filterBy = 'Location'; break
      case 3: this.state.filterBy = 'Shelf'; break
      case 4: this.state.filterBy = 'User'; break
      default:
        break
    }
    this.props.navigation.navigate('SettingsClearData', {
      clearRecordsBy: this.state.filterBy
    })
  };

  onLayout(e) {
    const { width, height } = Dimensions.get('window')
    this.setState({ screenWidth: width, screenHeight: height })
  }

  render() {
    let landscape = this.state.screenWidth > this.state.screenHeight

    return (
      <View
        onLayout={this.onLayout.bind(this)}
        style={Styles.mainView}
      >
        <View style={[Styles.mainViewHeader, landscape ? { height: 15 } : null]}></View>
        <FlatList style={Styles.mainList}
          data={[
            { key: 'Branch' },
            { key: 'Date' },
            { key: 'Location' },
            { key: 'Shelf' },
            { key: 'User' },
          ]}
          renderItem={({ item, index, separators }) => (
            <TouchableHighlight
              onPress={() => this._onPress(index)}
              onShowUnderlay={separators.highlight}
              onHideUnderlay={separators.unhighlight}
            >
              <View style={Styles.mainListItem}>
                <Text style={Styles.mainListText}>{item.key}</Text>
              </View>
            </TouchableHighlight>
          )}
        />
      </View>
    )
  }
}
