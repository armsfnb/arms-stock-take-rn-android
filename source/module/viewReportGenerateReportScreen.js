import React, { Component } from 'react';
import {
  Platform,
  View,
  ScrollView,
  Text,
  // TextInput,
  // Switch,
  Alert,
  // AsyncStorage,
  TouchableOpacity,
  Picker,
} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import { DatePickerDialog } from 'react-native-datepicker-dialog';
import Moment from 'moment'
import Styles from '../styles'
import SQLite from './sqlite'

var sqLite = new SQLite();

export default class ViewReportGenerateReportScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      branch: 'All', dateText: 'All', location: 'All', shelf: 'All', user: 'All',
      selectAll: true, prevCriteria: '',
      dateHolder: null
    }
    this.selectCriteria = this.selectCriteria.bind(this)
  }

  static navigationOptions = ({ navigation, navigationOptions }) => {
    const params = navigation.state.params || {};
    return {
      title: 'Generate Report',
      headerRight: (
        <TouchableOpacity onPress={() => {
          params.handleAlert()
        }}>
          <View style={Styles.headerButtonPosition}>
            <Text style={Styles.headerTextPosition}>
              All
            </Text>
          </View>
        </TouchableOpacity>
      ),
    };
  };

  componentWillMount() {
    sqLite.open().transaction((tx) => {
      tx.executeSql("select itemCode from stockTakeRecord limit 1", [], (tx, results) => {
        if (results.rows.length < 1) {
          Alert.alert(
            'No record', 'Please create new Stock Take record before viewing report.',
            [{ text: 'OK', onPress: () => this.props.navigation.goBack() }],
            { cancelable: false }
          )
        }
      });
    }, (error) => {
      console.log(error);
    });
  }

  componentDidMount() {
    this.props.navigation.setParams({ handleAlert: this._barBtnAllTapped });
  }

  selectCriteria = (criteria) => {
    if (this.state.selectAll && this.state.prevCriteria != '') {
      switch (this.state.prevCriteria) {
        case 'Branch': this.state.branch = 'All'; break
        case 'Location': this.state.location = 'All'; break
        case 'Shelf': this.state.shelf = 'All'; break
        case 'User': this.state.user = 'All'; break
        default: break
      }
    }

    this.state.selectAll = false
    var selectedCriteria
    switch (criteria) {
      case 'Branch': selectedCriteria = 'Branch'; break
      case 'Location': selectedCriteria = 'Location'; break
      case 'Shelf': selectedCriteria = 'Shelf'; break
      case 'User': selectedCriteria = 'User'; break
      default: selectCriteria = 'All'; break
    }
    this.props.navigation.navigate('ViewReportPicker', { criteria: selectedCriteria })
  }

  datePickerMainFunctionCall = () => {
    let dateHolder = this.state.dateHolder

    if (!dateHolder || dateHolder == null) {
      dateHolder = new Date();
      this.setState({
        dateHolder: dateHolder
      })
    }
    this.refs.DatePickerDialog.open({
      date: dateHolder,
      maxDate: new Date()
    })
  }

  onDatePickedFunction = (date) => {
    this.setState({
      dobDate: date,
      dateText: Moment(date).format('DD/MM/YYYY')
    })
  }

  _onPress = () => {
    this.props.navigation.navigate('ViewReportSTReport', {
      selectedBranch: this.state.branch,
      selectedDate: this.state.dateText,
      selectedLocation: this.state.location,
      selectedShelf: this.state.shelf,
      selectedUser: this.state.user,
    })
  }

  _barBtnAllTapped = () => {
    this.setState({
      branch: 'All', dateText: 'All', location: 'All', shelf: 'All', user: 'All',
      dateHolder: null, selectAll: true
    })

    this.props.navigation.setParams({
      selectedElement: 'All', selectedCriteria: ''
    })
  }

  render() {
    const { navigation } = this.props;
    var element
    var criteria

    if (this.state.selectAll != true) {
      element = navigation.getParam('selectedElement', 'All')
      criteria = navigation.getParam('selectedCriteria', '')
    } else {
      element = 'All'
      criteria = ''
    }

    this.state.prevCriteria = criteria

    switch (criteria) {
      case 'Branch': this.state.branch = element; break
      case 'Location': this.state.location = element; break
      case 'Shelf': this.state.shelf = element; break
      case 'User': this.state.user = element; break
      default: break
    }

    return (
      <View style={Styles.VRGenerateReportView}>

        <ScrollView style={{ width: '100%' }}>
          <View style={Styles.mainViewHeader}></View>

          {/* Branch */}
          <View style={Styles.VRGenerateReportRow}>
            <Text style={Styles.lblVRGenerateReport}>Branch</Text>
            <TouchableOpacity
              style={Styles.btnVRGenerateReportCriteria}
              onPress={() => this.selectCriteria('Branch')}
              ref="VRGenerateReportBranch"
            >
              <Text style={Styles.strVRGenerateReportCriteria}>{this.state.branch}</Text>
            </TouchableOpacity>
          </View>

          {/* Select Date */}
          <View style={Styles.VRGenerateReportRow}>
            <Text style={Styles.lblVRGenerateReport}>Date</Text>
            <TouchableOpacity
              style={Styles.btnVRGenerateReportDate}
              onPress={this.datePickerMainFunctionCall.bind(this)}
            >
              <Text style={Styles.strVRGenerateReportDate}>{this.state.dateText}</Text>
            </TouchableOpacity>
            <DatePickerDialog
              ref="DatePickerDialog"
              onDatePicked={this.onDatePickedFunction.bind(this)}
            />
          </View>

          {/* Location */}
          <View style={Styles.VRGenerateReportRow}>
            <Text style={Styles.lblVRGenerateReport}>Location</Text>
            <TouchableOpacity
              style={Styles.btnVRGenerateReportCriteria}
              onPress={() => this.selectCriteria('Location')}
            >
              <Text style={Styles.strVRGenerateReportCriteria}>{this.state.location}</Text>
            </TouchableOpacity>
          </View>

          {/* Shelf */}
          <View style={Styles.VRGenerateReportRow}>
            <Text style={Styles.lblVRGenerateReport}>Shelf</Text>
            <TouchableOpacity
              style={Styles.btnVRGenerateReportCriteria}
              onPress={() => this.selectCriteria('Shelf')}
            >
              <Text style={Styles.strVRGenerateReportCriteria}>{this.state.shelf}</Text>
            </TouchableOpacity>
          </View>

          {/* User */}
          <View style={Styles.VRGenerateReportRow}>
            <Text style={Styles.lblVRGenerateReport}>User</Text>
            <TouchableOpacity
              style={Styles.btnVRGenerateReportCriteria}
              onPress={() => this.selectCriteria('User')}
            >
              <Text style={Styles.strVRGenerateReportCriteria}>{this.state.user}</Text>
            </TouchableOpacity>
          </View>

          {/* Generate Report */}
          <TouchableOpacity onPress={() => this._onPress()}>
            <Text style={Styles.btnVRGenerateReport}>
              Generate
          </Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}