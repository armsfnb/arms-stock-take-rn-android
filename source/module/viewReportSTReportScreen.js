import React, { Component } from 'react';
import {
  SectionList,
  StyleSheet,
  Text,
  View,
  Image,
  Alert,
  PermissionsAndroid, // Storage permission
  AsyncStorage,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';
import Mailer from 'react-native-mail'
// import MailCompose from 'react-native-mail-compose'
import RNFetchBlob from 'react-native-fetch-blob'
import Moment from 'moment'
import { zip, unzip, unzipAssets, subscribe } from 'react-native-zip-archive'
import Styles from '../styles'
import SQLite from './sqlite'

var sqLite = new SQLite();
let stockTakeDir = '/storage/emulated/0/ARMSStockTake'

export default class ViewReportSTReportScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stockTakeRecords: [],
      stockTakeGroupByLocShelf: [],
      headerPreviewText: 'Code,Quantity',
      delimiter: ',',
      hasStoragePermission: true,
    }
  }

  static navigationOptions = ({ navigation, navigationOptions }) => {
    const params = navigation.state.params || {};
    return {
      title: 'Report',
      // Export Stock Take
      headerRight: (
        <TouchableOpacity
          disabled={!PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)}
          onPress={() => params.handleAlert()}>
          <View style={Styles.headerButtonPosition}>
            <Image
              source={require('../../Assets/export.png')}
              style={Styles.headerButton}
            />
          </View>
        </TouchableOpacity>
      ),
    };
  };

  componentWillMount() {
    this.props.navigation.setParams({ handleAlert: this.handleEmail });
    this.fetchExportFileFormat()
    this.checkExternalStoragePermission()
  }

  componentDidMount() {
    this.fetchStockTakeRecord()
  }

  componentWillUnmount() {
    // clear all files from the folder containing zip file and Stock Take files
    RNFetchBlob.fs.unlink(`${stockTakeDir}/stock_take/`)
    // RNFetchBlob.fs.unlink(RNFetchBlob.fs.dirs.DownloadDir)
  }

  async fetchExportFileFormat() {
    try {
      const getHeaderPrevText = await AsyncStorage.getItem('@headerSetting')
      const getDelimiter = await AsyncStorage.getItem('@delimiter')


      if (!(getHeaderPrevText == null || getDelimiter == null)) {
        this.setState({
          headerPreviewText: getHeaderPrevText,
          delimiter: getDelimiter
        })
      }
    } catch (error) {
      console.log("Error retrieving data" + error)
    }
  }

  async checkExternalStoragePermission() {
    try {
      const granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
      if (granted) {
        console.log("You can use the external storage")
        this.setState({ hasStoragePermission: true })
      } else {
        console.log("External storage permission denied")
        this.setState({ hasStoragePermission: false })
      }
    } catch (err) {
      console.warn(err)
    }
  }

  // Export Stock Take
  handleEmail = () => {
    if (this.state.hasStoragePermission == false) {
      alert("Please allow Storage Permission in your device settings and refresh this screen.")
    } else {

      /* Attach Stock Take records grouped by 'Location > Shelf'combinations (zip file) */
      var stPathToWriteList = []
      // const stHeaderString = 'Code,Quantity\n';
      const stHeaderString = `${this.state.headerPreviewText}\n`;
      const stFieldList = this.state.headerPreviewText.split(this.state.delimiter)

      // Generate list of LocShelfTitles
      var locShelfTitleList = []
      var jLength = this.state.stockTakeGroupByLocShelf.length

      for (let j = 0; j < jLength; j++) {
        if (this.state.stockTakeGroupByLocShelf[j].title != '') {
          locShelfTitleList.push(this.state.stockTakeGroupByLocShelf[j].title)
        }
      }

      for (let k = 0; k < locShelfTitleList.length; k++) {
        const stockTakeValuesInList = []
        const stockTakeLenInList = locShelfTitleList.length

        if (this.state.stockTakeGroupByLocShelf[k].data.length != 0) {

          var stRowStringInList = ''
          for (let i = 0; i < this.state.stockTakeGroupByLocShelf[k].data.length; i++) {
            var arrValue = ''
            var itemDetail

            for (let j = 0; j < stFieldList.length; j++) {
              switch (stFieldList[j]) {
                case "Code":
                  itemDetail = this.state.stockTakeGroupByLocShelf[k].data[i].key.itemCode
                  break;
                case "Quantity":
                  itemDetail = this.state.stockTakeGroupByLocShelf[k].data[i].key.itemQty
                  break;
                case "Branch":
                  itemDetail = this.state.stockTakeGroupByLocShelf[k].data[i].key.branch
                  break;
                case "Date":
                  itemDetail = this.state.stockTakeGroupByLocShelf[k].data[i].key.dateStockTake
                  break;
                case "Location":
                  itemDetail = this.state.stockTakeGroupByLocShelf[k].data[i].key.location
                  break;
                case "Shelf":
                  itemDetail = this.state.stockTakeGroupByLocShelf[k].data[i].key.shelf
                  break;
                default:
                  break;
              }
              if (arrValue == '') {
                arrValue = itemDetail
              } else {
                arrValue = arrValue + this.state.delimiter + itemDetail
              }
            }

            stRowStringInList = stRowStringInList + arrValue + "\n";
          }
        }

        const stCsvStringInList = `${stHeaderString}${stRowStringInList}`;

        const curDateStr = Moment(new Date()).format('DDMMYYYY');
        var locShelfReplaced = locShelfTitleList[k].replace(" > ", "_");
        for (m = 0; m < locShelfReplaced.length; m++) {
          locShelfReplaced = locShelfReplaced.replace(" ", "-");
        }

        const stPathToWriteInList = `${stockTakeDir}/stock_take/ST_${locShelfReplaced}.csv`;
        // const stPathToWriteInList = `${RNFetchBlob.fs.dirs.DownloadDir}/stock_take/ST_${locShelfReplaced}.csv`;
        // /storage/emulated/0/logs

        RNFetchBlob.fs
          .writeFile(stPathToWriteInList, stCsvStringInList, 'utf8')
          .then(() => {
            console.log(`wrote file ${stPathToWriteInList}`);
            // wrote file /storage/emulated/0/Download/data.csv
          })
          .catch(error => {
            alert("no directory found")
            console.error(error)
          });
      }

      // Folder path one layer higher than the source path
      const targetPath = `${stockTakeDir}/ST_${Moment(new Date()).format('DDMMYYYY')}.zip`
      // const targetPath = `${RNFetchBlob.fs.dirs.DownloadDir}/ST_${Moment(new Date()).format('DDMMYYYY')}.zip`
      
      // Folder path with all Stock Take files (must be separate folder from zip file)
      const sourcePath = `${stockTakeDir}/stock_take`
      // const sourcePath = `${RNFetchBlob.fs.dirs.DownloadDir}/stock_take`

      zip(sourcePath, targetPath)
        .then((path) => {
          console.log(`zip completed at ${path}`)
          // Generate email
          Mailer.mail({
            subject: `ST_${Moment(new Date()).format('DDMMYYYY')}`,
            recipients: [],
            body: '',
            isHTML: true,
            attachment: {
              path: path,
              name: `ST_${Moment(new Date()).format('DDMMYYYY')}.zip`,
              type: '',
            }
          }, (error, event) => {
            Alert.alert(
              error,
              event,
              [
                { text: 'Ok', onPress: () => console.log('OK: Email Error Response') },
                { text: 'Cancel', onPress: () => console.log('CANCEL: Email Error Response') }
              ],
              { cancelable: true }
            )
          });
        })
        .catch((error) => {
          console.log(error)
        })
    }

    // Sample: Write CSV file: https://stackoverflow.com/questions/45660768/how-to-create-and-save-csv-files-in-react-native?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
    // Sample: Generate email: https://github.com/chirag04/react-native-mail
  }

  fetchStockTakeRecord() {
    const { params } = this.props.navigation.state

    const selBranch = params.selectedBranch
    const selDate = params.selectedDate
    const selLocation = params.selectedLocation
    const selShelf = params.selectedShelf
    const selUser = params.selectedUser

    var selectQuery = `select * from stockTakeRecord`

    // Stock Take Query
    if (selBranch != 'All') {
      selectQuery = selectQuery + ` where branch = '${selBranch}'`
    }

    if (selDate != 'All') {
      if (selectQuery.includes('where') == false) {
        selectQuery = selectQuery + ` where dateStockTake = '${selDate}'`
      } else {
        selectQuery = selectQuery + ` and dateStockTake = '${selDate}'`
      }
    }

    if (selLocation != 'All') {
      if (selectQuery.includes('where') == false) {
        selectQuery = selectQuery + ` where location = '${selLocation}'`
      } else {
        selectQuery = selectQuery + ` and location = '${selLocation}'`
      }
    }
    if (selShelf != 'All') {
      if (selectQuery.includes('where') == false) {
        selectQuery = selectQuery + ` where shelf = '${selShelf}'`
      } else {
        selectQuery = selectQuery + ` and shelf = '${selShelf}'`
      }
    }
    if (selUser != 'All') {
      if (selectQuery.includes('where') == false) {
        selectQuery = selectQuery + ` where user = '${selUser}'`
      } else {
        selectQuery = selectQuery + ` and user = '${selUser}'`
      }
    }

    sqLite.open().transaction((tx) => {
      tx.executeSql(selectQuery, [], (tx, results) => {
        var len = results.rows.length;

        if (len == 0) {
          Alert.alert(
            'No record', '- Please create new Stock Take record.\n- Please regenerate the report.',
            [{ text: 'OK', onPress: () => this.props.navigation.goBack() }],
            { cancelable: false }
          )
        }

        // Group Stock Take records based on unique combinations of "Location > Shelf"
        for (let i = 0; i < len; i++) {
          var st = results.rows.item(i);
          this.state.stockTakeRecords.push(st)

          // Create new LocShelfTitle
          const locShelfTitle = st.location + ' > ' + st.shelf

          // Generate array of existing LocShelfTitles
          var locShelfTitleList = []
          var jLength = this.state.stockTakeGroupByLocShelf.length
          var keyItem = { key: st }
          for (let j = 0; j < jLength; j++) {
            locShelfTitleList.push(this.state.stockTakeGroupByLocShelf[j].title)
          }
          // LocShelf combination not found, add title and data
          if (locShelfTitleList.indexOf(locShelfTitle) == -1) {
            this.state.stockTakeGroupByLocShelf.push({ title: locShelfTitle, data: [keyItem] })
          } else {
            const index = locShelfTitleList.indexOf(locShelfTitle)
            this.state.stockTakeGroupByLocShelf[index].data.push(keyItem)
          }
        }
        const list = this.state.stockTakeGroupByLocShelf
        this.setState({
          stockTakeGroupByLocShelf: list
        })

      });
    }, (error) => {
      console.log(error);
    });
    // SectionList: https://facebook.github.io/react-native/docs/sectionlist.html
  }

  _onPress = (stItem, rowIndex, sectionIndex) => {
    this.props.navigation.navigate('ViewReportSTDetails', {
      item: stItem,
      index: rowIndex,
      section: sectionIndex.title
    });
  };

  render() {
    const { navigation } = this.props;

    const selectedBranch = navigation.getParam('selectedBranch', 'All')
    const selectedDate = navigation.getParam('selectedDate', 'All')
    const selectedLocation = navigation.getParam('selectedLocation', 'All')
    const selectedShelf = navigation.getParam('selectedShelf', 'All')
    const selectedUser = navigation.getParam('selectedUser', 'All')

    const updatedQty = navigation.getParam('updatedQty', 0)

    const updatedRow = navigation.getParam('updatedRow', -1)
    const updatedSec = navigation.getParam('updatedSec', '')

    /* Note: compare row index and section title to update qty at correct position. */
    const len = this.state.stockTakeGroupByLocShelf.length
    if (updatedRow != -1) {
      for (let i = 0; i < len; i++) {
        if (updatedSec == this.state.stockTakeGroupByLocShelf[i].title) {
          this.state.stockTakeGroupByLocShelf[i].data[updatedRow].key.itemQty = updatedQty
          break
        }
      }
    }

    return (
      <View style={Styles.viewReportContainer}>
        <View style={{ flexDirection: 'row', height: 25, justifyContent: 'center' }}>
          {/* Report header */}
          <Text style={[Styles.sectionListHeader, Styles.sectionListCode]}>CODE</Text>
          <Text style={[Styles.sectionListHeader, Styles.sectionListQty]}>QTY</Text>
        </View>

        {/* Report view */}
        <SectionList
          sections={this.state.stockTakeGroupByLocShelf}
          renderItem={({ item, index, section, separators }) => (
            <TouchableHighlight
              onPress={() => this._onPress(item.key, index, section)}
              onShowUnderlay={separators.highlight}
              onHideUnderlay={separators.unhighlight}
            >
              <View style={{ flexDirection: 'row' }}>
                <Text style={[Styles.sectionListItem, Styles.sectionListCode]}>
                  {item.key.itemCode}
                </Text>
                <Text style={[Styles.sectionListItem, Styles.sectionListQty]}>
                  {item.key.itemQty}
                </Text>
              </View>
            </TouchableHighlight>
          )}
          renderSectionHeader={({ section }) => (
            <Text style={Styles.sectionListHeader}>
              {section.title}
            </Text>
          )}
        />
      </View>
    );
  }
}
