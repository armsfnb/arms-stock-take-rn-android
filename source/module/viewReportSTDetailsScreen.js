import React, { Component } from 'react';
import {
  Platform,
  View,
  ScrollView,
  Text,
  TextInput,
  // Switch,
  Alert,
  AsyncStorage,
  TouchableOpacity,
  Image,
} from 'react-native';
import { createStackNavigator } from 'react-navigation';
// import SimpleStepper from 'react-native-simple-stepper'
// import Moment from 'moment'
import Styles from '../styles'
import SQLite from './sqlite'

var sqLite = new SQLite();

export default class ViewReportSTDetailsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stockID: 'Stock ID', quantity: '', branch: 'Branch', dateText: 'Date', location: 'Location', shelf: 'Shelf', user: 'User',
      stockTakeRecordList: [], activeResetSave: false,
    }
  }

  static navigationOptions = ({ navigation, navigationOptions }) => {
    const params = navigation.state.params || {};
    return {
      title: 'Stock Take Details',
      headerRight: (
        <TouchableOpacity onPress={() => params.handleAlert()}>
          <View style={Styles.headerButtonPosition}>
            <Image
              source={require('../../Assets/trash_can.png')}
              style={Styles.headerButton}
            />
          </View>
        </TouchableOpacity>
      ),
    };
  };

  componentWillMount() {
    const { navigation } = this.props;

    const detailItem = navigation.getParam('item', null)
    this.setState({
      stockID: detailItem.itemCode,
      quantity: JSON.stringify(detailItem.itemQty),
      branch: detailItem.branch,
      dateText: detailItem.dateStockTake,
      location: detailItem.location,
      shelf: detailItem.shelf,
      user: detailItem.user,
    })
    this.props.navigation.setParams({ handleAlert: this.removeStockTakeRecord });
  }

  // Save Button pressed
  saveNewQty = () => {
    Alert.alert(
      'Saved', 'Stock Take item qty updated.',
      [{ text: 'OK' }],
      { cancelable: false }
    )

    const { navigation } = this.props;

    const detailItem = navigation.getParam('item', null)
    const rowIndex = navigation.getParam('index', -1)
    const sectionTitle = navigation.getParam('section', '')

    // load Stock Take records
    sqLite.open().transaction((tx) => {
      tx.executeSql(`update stockTakeRecord set itemQty = ${this.state.quantity} where dateSaved = '${detailItem.dateSaved}'`, [], (tx, results) => {
      });
    }, (error) => {
      console.log(error);
    });
    this.props.navigation.navigate('ViewReportSTReport', {
      updatedQty: parseInt(this.state.quantity),
      // updatedQty: this.state.quantity,
      updatedRow: rowIndex,
      updatedSec: sectionTitle
    })
  }

  // Header button "Delete" pressed
  removeStockTakeRecord = () => {
    const { navigation } = this.props;

    const detailItem = navigation.getParam('item', null)
    const rowIndex = navigation.getParam('index', -1)
    const sectionTitle = navigation.getParam('section', '')

    Alert.alert(
      'Delete record?', 'This action cannot be undone.',
      [
        { text: 'Cancel', style: 'cancel' },
        {
          text: 'Delete', style: 'destructive', onPress: () => {
            // Delete Stock Take record
            sqLite.open().transaction((tx) => {
              tx.executeSql(`delete from stockTakeRecord where dateSaved = '${detailItem.dateSaved}'`, [], (tx, results) => {
                this.removeStockTakeList()
                this.saveStockTakeList()
              });
            }, (error) => { console.log(error); });

            Alert.alert(
              'Record deleted', 'Please regenerate the report.',
              [{ text: 'OK', onPress: () => this.props.navigation.navigate('ViewReportGenerateReport') }],
              { cancelable: false }
            )
          }
        }
      ],
      { cancelable: false }
    )

    // this.props.navigation.navigate('ViewReportGenerateReport')

    // this.props.navigation.navigate('ViewReportSTReport', {
    //   deletedRow: rowIndex,
    //   deletedSec: sectionTitle
    // })
  }

  generateStockTakeList() {
    sqLite.open().transaction((tx) => {
      tx.executeSql(`select * from stockTakeRecord`, [], (tx, results) => {
        var len = results.rows.length;
        for (let i = 0; i < len; i++) {
          var u = results.rows.item(i);
          this.state.stockTakeRecordList.push(u)
        }
        this.saveStockTakeList()
      });
    }, (error) => {
      console.log(error);
    });
  }

  async removeStockTakeList() {
    this.generateStockTakeList()
    try {
      await AsyncStorage.removeItem('@branchList')
      await AsyncStorage.removeItem('@dateList')
      await AsyncStorage.removeItem('@locationList')
      await AsyncStorage.removeItem('@shelfList')
      await AsyncStorage.removeItem('@userList')

    } catch (error) {
      console.log("Error removing data" + error)
    }
  }

  async saveStockTakeList() {
    const newBranchList = []
    const newDateList = []
    const newLocationList = []
    const newShelfList = []
    const newUserList = []

    for (let i = 0; i < this.state.stockTakeRecordList.length; i++) {
      var u = this.state.stockTakeRecordList[i];

      if (newBranchList.indexOf(u.branch) == -1) {
        newBranchList.push(u.branch)
      }
      if (newDateList.indexOf(u.dateStockTake) == -1) {
        newDateList.push(u.dateStockTake)
      }
      if (newLocationList.indexOf(u.location) == -1) {
        newLocationList.push(u.location)
      }
      if (newShelfList.indexOf(u.shelf) == -1) {
        newShelfList.push(u.shelf)
      }
      if (newUserList.indexOf(u.user) == -1) {
        newUserList.push(u.user)
      }
    }
    const newBranchListString = JSON.stringify(newBranchList)
    const newDateListString = JSON.stringify(newDateList)
    const newLocationListString = JSON.stringify(newLocationList)
    const newShelfListString = JSON.stringify(newShelfList)
    const newUserListString = JSON.stringify(newUserList)

    try {
      await AsyncStorage.setItem('@branchList', newBranchListString)
      await AsyncStorage.setItem('@dateList', newDateListString)
      await AsyncStorage.setItem('@locationList', newLocationListString)
      await AsyncStorage.setItem('@shelfList', newShelfListString)
      await AsyncStorage.setItem('@userList', newUserListString)
    } catch (error) {
      console.log("Error saving data" + error)
    }
  }

  render() {
    const { navigation } = this.props;

    const detailItem = navigation.getParam('item', null)

    return (
      <View style={Styles.VRGenerateReportView}>

        <ScrollView style={{ width: '100%' }}>
          <View style={Styles.mainViewHeader}></View>

          {/* Stock ID */}
          <View style={Styles.stDetailsRow}>
            <Text style={Styles.lblVRGenerateReport}>Stock ID</Text>
            <Text style={Styles.stDetailsInfo} numberOfLines={1}>{this.state.stockID}</Text>
          </View>

          {/* Quantity */}
          <View style={Styles.stDetailsRow}>
            <Text style={Styles.lblVRGenerateReport}>Quantity</Text>
            <TextInput
              style={Styles.txtStDetails}
              placeholder="Enter qty here"
              keyboardType='numeric'
              returnKeyType='done'
              // disable Reset and Save button when focus
              onFocus={(quantity) => {
                this.setState({ activeResetSave: false })
              }}
              onChangeText={(quantity) => {
                this.setState({ quantity: quantity.replace(/[^0-9]/g, '') })
                // this.state.quantity = quantity.replace(/[^0-9]/g, '')
              }}
              onSubmitEditing={(quantity) => {
                var qtyInt
                if (this.state.quantity == '') {
                  qtyInt = 1
                } else {
                  qtyInt = parseInt(this.state.quantity)
                  if (qtyInt < 1) {
                    qtyInt = 1
                  }
                }
                this.setState({ quantity: String(qtyInt), activeResetSave: true })
              }}
              value={String(this.state.quantity)}
            />
          </View>

          {/* Branch */}
          <View style={Styles.stDetailsRow}>
            <Text style={Styles.lblVRGenerateReport}>Branch</Text>
            <Text style={Styles.stDetailsInfo} numberOfLines={1}>{this.state.branch}</Text>
          </View>

          {/* Date */}
          <View style={Styles.stDetailsRow}>
            <Text style={Styles.lblVRGenerateReport}>Date</Text>
            <Text style={Styles.stDetailsInfo} numberOfLines={1}>{this.state.dateText}</Text>
          </View>

          {/* Location */}
          <View style={Styles.stDetailsRow}>
            <Text style={Styles.lblVRGenerateReport}>Location</Text>
            <Text style={Styles.stDetailsInfo} numberOfLines={1}>{this.state.location}</Text>
          </View>

          {/* Shelf */}
          <View style={Styles.stDetailsRow}>
            <Text style={Styles.lblVRGenerateReport}>Shelf</Text>
            <Text style={Styles.stDetailsInfo} numberOfLines={1}>{this.state.shelf}</Text>
          </View>

          {/* User */}
          <View style={Styles.stDetailsRow}>
            <Text style={Styles.lblVRGenerateReport}>User</Text>
            <Text style={Styles.stDetailsInfo} numberOfLines={1}>{this.state.user}</Text>
          </View>

          {/* Reset */}
          <View style={[Styles.stDetailsRow, { justifyContent: 'center', marginBottom: 30 }]}>
            <TouchableOpacity
              disabled={this.state.activeResetSave ? false : true}
              onPress={() => this.setState({ quantity: detailItem.itemQty, activeResetSave: false })}
            // onPress={() => this.resetQty()}
            >
              <Text
                style={[Styles.btnStDetails, { backgroundColor: 'red' },
                (this.state.activeResetSave) ? Styles.btnEnabled : Styles.btnDisabled]}
              >
                Reset
            </Text>
            </TouchableOpacity>

            {/* Save */}
            <TouchableOpacity
              disabled={this.state.activeResetSave ? false : true}
              onPress={() => this.saveNewQty()}
            >
              <Text
                style={[Styles.btnStDetails,
                (this.state.activeResetSave) ? Styles.btnEnabled : Styles.btnDisabled]}
              >
                Save
            </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View >
    );
  }
}

