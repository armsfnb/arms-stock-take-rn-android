import React, { Component } from 'react';
import {
  Platform,
  View,
  ScrollView,
  Dimensions,
  Text,
  Alert,
  Switch,
  StyleSheet,
  AsyncStorage,
  TouchableOpacity,
} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Swiper from 'react-native-swiper'
import Styles from '../styles'
import SQLite from './sqlite'
import SegmentedControlTab from 'react-native-segmented-control-tab'
import DeviceInfo from 'react-native-device-info'

var sqLite = new SQLite();

let INDEX_CODE = 1;
let INDEX_QUANTITY = 2;
let INDEX_BRANCH = 3;
let INDEX_DATE = 4;
let INDEX_LOCATION = 5;
let INDEX_SHELF = 6;

let IS_NQUIRE_PRICE_CHECKER = (DeviceInfo.getModel() == "NQuire 300")

export default class SettingsExportFileFormatSetupScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      headerPreviewText: 'Code,Quantity',
      delimiter: ",",
      hasCode: true, hasQuantity: true, hasBranch: false,
      hasDate: false, hasLocation: false, hasShelf: false,
      selectedIndex: 0,
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,

    }
    this.fetchExportFileFormat()
  }

  static navigationOptions = ({ navigation, navigationOptions }) => {
    const params = navigation.state.params || {};
    return {
      title: 'Export File Format',
      headerRight: (
        <TouchableOpacity onPress={() => {
          params.handleAlert()
        }}>
          <View style={[Styles.headerButtonPosition, { width: 60 }]}>
            <Text style={Styles.headerTextPosition}>
              Save
            </Text>
          </View>
        </TouchableOpacity>
      ),
    };
  }

  componentDidMount() {
    this.props.navigation.setParams({ handleAlert: this._barBtnSaveTapped });
    // this.fetchExportFileFormat()
    // this.fetchButtonState()
  }

  async fetchExportFileFormat() {
    try {
      const getHeaderPrevText = await AsyncStorage.getItem('@headerSetting')
      const getDelimiter = await AsyncStorage.getItem('@delimiter')

      if (!(getHeaderPrevText == null || getDelimiter == null)) {
        this.setState({
          headerPreviewText: getHeaderPrevText,
          delimiter: getDelimiter
        })
      }
    } catch (error) {
      console.log("Error retrieving data" + error)
    }

    this.fetchButtonState()
  }

  async saveExportFileFormat() {
    try {
      await AsyncStorage.setItem('@headerSetting', this.state.headerPreviewText);
      await AsyncStorage.setItem('@delimiter', this.state.delimiter);
    } catch (error) {
      console.log("Error saving data" + error)
    }
  }

  fetchButtonState() {
    const columnList = this.state.headerPreviewText
    const del = this.state.delimiter

    // alert(columnList)

    if (columnList.indexOf("Code") !== -1) {
      this.setState({ hasCode: true })
    }
    if (columnList.indexOf("Quantity") !== -1) {
      this.setState({ hasQuantity: true })
    }
    if (columnList.indexOf("Branch") !== -1) {
      this.setState({ hasBranch: true })
    }
    if (columnList.indexOf("Date") !== -1) {
      this.setState({ hasDate: true })
    }
    if (columnList.indexOf("Location") !== -1) {
      this.setState({ hasLocation: true })
    }
    if (columnList.indexOf("Shelf") !== -1) {
      this.setState({ hasShelf: true })
    }

    switch (del) {
      case ",": this.setState({ selectedIndex: 0 }); break
      case "|": this.setState({ selectedIndex: 1 }); break
      case ";": this.setState({ selectedIndex: 2 }); break
      case " ": this.setState({ selectedIndex: 3 }); break
      default: break
    }
  }

  _barBtnSaveTapped = () => {
    if (!(this.state.hasCode && this.state.hasQuantity)) {
      alert("Export file must contain Code and Quantity.")
    } else {
      this.saveExportFileFormat()
      Alert.alert(
        'Saved',
        'The export file format is saved.',
        [{ text: 'OK', onPress: () => this.props.navigation.goBack() }],
        { cancelable: false }
      )
    }
  }

  _clearHeader = () => {
    this.setState({
      headerPreviewText: '',
      selectedIndex: 0,
      delimiter: ",",
      hasCode: false, hasQuantity: false, hasBranch: false,
      hasDate: false, hasLocation: false, hasShelf: false,
    })

  }

  _headerTitleClicked = (headerIndex) => {
    var text = this.state.headerPreviewText
    switch (headerIndex) {
      case INDEX_CODE:
        if (this.state.headerPreviewText == '') { text = "Code" }
        else { text = text + this.state.delimiter + "Code" }
        this.setState({ headerPreviewText: text, hasCode: true })
        break;
      case INDEX_QUANTITY:
        if (this.state.headerPreviewText == '') { text = "Quantity" }
        else { text = text + this.state.delimiter + "Quantity" }
        this.setState({ headerPreviewText: text, hasQuantity: true })
        break;
      case INDEX_BRANCH:
        if (this.state.headerPreviewText == '') { text = "Branch" }
        else { text = text + this.state.delimiter + "Branch" }
        this.setState({ headerPreviewText: text, hasBranch: true })
        break;
      case INDEX_DATE:
        if (this.state.headerPreviewText == '') { text = "Date" }
        else { text = text + this.state.delimiter + "Date" }
        this.setState({ headerPreviewText: text, hasDate: true })
        break;
      case INDEX_LOCATION:
        if (this.state.headerPreviewText == '') { text = "Location" }
        else { text = text + this.state.delimiter + "Location" }
        this.setState({ headerPreviewText: text, hasLocation: true })
        break;
      case INDEX_SHELF:
        if (this.state.headerPreviewText == '') { text = "Shelf" }
        else { text = text + this.state.delimiter + "Shelf" }
        this.setState({ headerPreviewText: text, hasShelf: true })
        break;
      default:
        break;
    }
  }

  handleIndexChange = (index) => {
    var oldText = this.state.headerPreviewText
    var selDelimiter
    switch (index) {
      case 0: selDelimiter = ","; break
      case 1: selDelimiter = "|"; break
      case 2: selDelimiter = ";"; break
      case 3: selDelimiter = " "; break
      default: break;
    }
    for (m = 0; m < oldText.length; m++) {
      oldText = oldText.replace(this.state.delimiter, selDelimiter)
    }
    this.setState({
      ...this.state,
      headerPreviewText: oldText,
      selectedIndex: index,
      delimiter: selDelimiter
    })
  }

  onLayout(e) {
    const { width, height } = Dimensions.get('window')
    this.setState({ screenWidth: width, screenHeight: height })
  }

  render() {
    const { navigation } = this.props;
    const landscape = this.state.screenWidth > this.state.screenHeight

    return (
      <View onLayout={this.onLayout.bind(this)} style={Styles.StockTakeInfoView}>
        <ScrollView style={{ width: '100%' }}>

          <View style={[Styles.exportFileFormatSectionTitle,
          landscape ? { paddingTop: 10, paddingBottom: 2 } : null]}>
            <Text style={Styles.exportFileFormatTitleLbl}>Header Preview</Text>
          </View>

          <View style={[Styles.exportFileFormatHeaderPreview,
          landscape ? { height: 35 } : null]}>
            <Text style={{ fontFamily: 'monospace', }}>
              {this.state.headerPreviewText}
            </Text>
          </View>

          <TouchableOpacity onPress={() => this._clearHeader()}>
            <Text style={[Styles.exportFileFormatBtnClear, landscape ? { paddingTop: 5 } : null]}>Clear</Text>
          </TouchableOpacity>

          <View style={[Styles.exportFileFormatSectionTitle,
          landscape ? { paddingTop: 6, paddingBottom: 2 } : null]}>
            <Text style={Styles.exportFileFormatTitleLbl}>Header Library</Text>
          </View>

          {!landscape ?

            /*** A. Export File Format View in Portrait mode STARTS ***/
            < View >
              <View style={Styles.exportFileFormatHeaderLibrary}>
                <TouchableOpacity
                  onPress={() => this._headerTitleClicked(INDEX_CODE)}
                  disabled={this.state.hasCode}
                  style={{ flex: 1 }}>
                  <Text style={[Styles.exportFileFormatBtnLibrary,
                  this.state.hasCode ? Styles.btnDisabled : null]}>
                    Code
                </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this._headerTitleClicked(INDEX_QUANTITY)}
                  disabled={this.state.hasQuantity}
                  style={{ flex: 1 }}>
                  <Text style={[Styles.exportFileFormatBtnLibrary,
                  this.state.hasQuantity ? Styles.btnDisabled : null]}>
                    Quantity
                </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this._headerTitleClicked(INDEX_BRANCH)}
                  disabled={this.state.hasBranch}
                  style={{ flex: 1 }}>
                  <Text style={[Styles.exportFileFormatBtnLibrary,
                  this.state.hasBranch ? Styles.btnDisabled : null]}>
                    Branch
                </Text>
                </TouchableOpacity>
              </View>

              <View style={Styles.exportFileFormatHeaderLibrary}>
                <TouchableOpacity
                  onPress={() => this._headerTitleClicked(INDEX_DATE)}
                  disabled={this.state.hasDate}
                  style={{ flex: 1 }}>
                  <Text style={[Styles.exportFileFormatBtnLibrary,
                  this.state.hasDate ? Styles.btnDisabled : null]}>
                    Date
                </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this._headerTitleClicked(INDEX_LOCATION)}
                  disabled={this.state.hasLocation}
                  style={{ flex: 1 }}>
                  <Text style={[Styles.exportFileFormatBtnLibrary,
                  this.state.hasLocation ? Styles.btnDisabled : null]}>
                    Location
                </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this._headerTitleClicked(INDEX_SHELF)}
                  disabled={this.state.hasShelf}
                  style={{ flex: 1 }}>
                  <Text style={[Styles.exportFileFormatBtnLibrary,
                  this.state.hasShelf ? Styles.btnDisabled : null]}>
                    Shelf
                </Text>
                </TouchableOpacity>
              </View>
            </View>
            /*** A. Export File Format View in Portrait mode ENDS ***/
            :
            /*** B. Export File Format View in Landscape mode STARTS ***/
            IS_NQUIRE_PRICE_CHECKER ?

              /** B1. Export File Format View for Newland NQuire Price Checker STARTS **/
              <View style={[Styles.exportFileFormatHeaderLibrary, { height: 40 }]}>

                {/*** Swiper view ***/}
                <Swiper horizontal={false} loop={false}>

                  {/* Swiper View First Slide */}
                  <View style={[Styles.slider, { flexDirection: 'row' }]}>
                    <View style={{ width: 50 }}></View>
                    <TouchableOpacity
                      onPress={() => this._headerTitleClicked(INDEX_CODE)}
                      disabled={this.state.hasCode}
                      style={{ flex: 1 }}>
                      <Text
                        style={[Styles.exportFileFormatBtnLibrary,
                        this.state.hasCode ? Styles.btnDisabled : null]}>
                        Code
                    </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      onPress={() => this._headerTitleClicked(INDEX_QUANTITY)}
                      disabled={this.state.hasQuantity}
                      style={{ flex: 1 }}>
                      <Text
                        style={[Styles.exportFileFormatBtnLibrary,
                        this.state.hasQuantity ? Styles.btnDisabled : null]}>
                        Quantity
                    </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      onPress={() => this._headerTitleClicked(INDEX_BRANCH)}
                      disabled={this.state.hasBranch}
                      style={{ flex: 1 }}>
                      <Text
                        style={[Styles.exportFileFormatBtnLibrary,
                        this.state.hasBranch ? Styles.btnDisabled : null]}>
                        Branch
                    </Text>
                    </TouchableOpacity>
                    <View style={{ width: 50 }}></View>

                  </View>

                  {/* Swiper View Second Slide */}
                  <View style={[Styles.slider, { flexDirection: 'row' }]}>
                    <View style={{ width: 50 }}></View>

                    <TouchableOpacity
                      onPress={() => this._headerTitleClicked(INDEX_DATE)}
                      disabled={this.state.hasDate}
                      style={{ flex: 1 }}>
                      <Text
                        style={[Styles.exportFileFormatBtnLibrary,
                        this.state.hasDate ? Styles.btnDisabled : null]}>
                        Date
                    </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      onPress={() => this._headerTitleClicked(INDEX_LOCATION)}
                      disabled={this.state.hasLocation}
                      style={{ flex: 1 }}>
                      <Text
                        style={[Styles.exportFileFormatBtnLibrary,
                        this.state.hasLocation ? Styles.btnDisabled : null]}>
                        Location
                    </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      onPress={() => this._headerTitleClicked(INDEX_SHELF)}
                      disabled={this.state.hasShelf}
                      style={{ flex: 1 }}>
                      <Text
                        style={[Styles.exportFileFormatBtnLibrary,
                        this.state.hasShelf ? Styles.btnDisabled : null]}>
                        Shelf
                    </Text>
                    </TouchableOpacity>
                    <View style={{ width: 50 }}></View>

                  </View>

                </Swiper>
              </View>

              /** B1. Export File Format View for Newland NQuire Price Checker ENDS **/
              :
              /** B2. Export File Format View for other devices (Landscape) STARTS **/
              <View style={Styles.exportFileFormatHeaderLibrary}>
                <TouchableOpacity
                  onPress={() => this._headerTitleClicked(INDEX_CODE)}
                  disabled={this.state.hasCode}
                  style={{ flex: 1 }}>
                  <Text style={[Styles.exportFileFormatBtnLibrary,
                  this.state.hasCode ? Styles.btnDisabled : null]}>
                    Code
                </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this._headerTitleClicked(INDEX_QUANTITY)}
                  disabled={this.state.hasQuantity}
                  style={{ flex: 1 }}>
                  <Text style={[Styles.exportFileFormatBtnLibrary,
                  this.state.hasQuantity ? Styles.btnDisabled : null]}>
                    Quantity
                </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this._headerTitleClicked(INDEX_BRANCH)}
                  disabled={this.state.hasBranch}
                  style={{ flex: 1 }}>
                  <Text style={[Styles.exportFileFormatBtnLibrary,
                  this.state.hasBranch ? Styles.btnDisabled : null]}>
                    Branch
                </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this._headerTitleClicked(INDEX_DATE)}
                  disabled={this.state.hasDate}
                  style={{ flex: 1 }}>
                  <Text style={[Styles.exportFileFormatBtnLibrary,
                  this.state.hasDate ? Styles.btnDisabled : null]}>
                    Date
                </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this._headerTitleClicked(INDEX_LOCATION)}
                  disabled={this.state.hasLocation}
                  style={{ flex: 1 }}>
                  <Text style={[Styles.exportFileFormatBtnLibrary,
                  this.state.hasLocation ? Styles.btnDisabled : null]}>
                    Location
                </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this._headerTitleClicked(INDEX_SHELF)}
                  disabled={this.state.hasShelf}
                  style={{ flex: 1 }}>
                  <Text style={[Styles.exportFileFormatBtnLibrary,
                  this.state.hasShelf ? Styles.btnDisabled : null]}>
                    Shelf
                </Text>
                </TouchableOpacity>
              </View>
            /** B2. Export File Format View for other devices (Landscape) ENDS **/

            /*** B. Export File Format View in Landscape mode ENDS ***/
          }

          {IS_NQUIRE_PRICE_CHECKER ?
            <Text style={{ alignSelf: 'center', fontSize: 12 }}>Swipe left/right for more header fields.</Text>
            :
            null}

          <View style={[Styles.exportFileFormatSectionTitle,
          landscape ? { paddingTop: 6, paddingBottom: 2 } : null]}>
            <Text style={Styles.exportFileFormatTitleLbl}>Delimiter</Text>
          </View>

          <View style={Styles.exportFileFormatDelimiterRow}>
            <SegmentedControlTab
              values={[',', '|', ';', 'space']}
              selectedIndex={this.state.selectedIndex}
              onTabPress={this.handleIndexChange}
            />
          </View>

          <View style={{ height: 15 }}></View>
        </ScrollView>
      </View>
    );
  }
}
